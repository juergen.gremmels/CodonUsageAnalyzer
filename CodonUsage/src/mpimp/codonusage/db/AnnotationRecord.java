/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.codonusage.db;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class AnnotationRecord {

	public AnnotationRecord(String annotationName, Boolean isReverseComplement) {
		annotationName_ = annotationName;
		qualifiers_ = new ArrayList<String>();
		isReverseComplement_ = isReverseComplement;
		joinPartItems_ = new ArrayList<JoinPartItem>();
	}

	public String getAnnotationName() {
		return annotationName_;
	}

	public List<String> getQualifiers() {
		return qualifiers_;
	}

	public String getPartSequence() {
		return partSequence_;
	}

	public void setPartSequence(String partSequence) {
		partSequence_ = partSequence;
	}

	// public String getPartSequenceOriginal() {
	// return partSequenceOriginal_;
	// }
	//
	// public void setPartSequenceOriginal(String partSequenceOriginal) {
	// partSequenceOriginal_ = partSequenceOriginal;
	// }

	public Boolean getIsReverseComplement() {
		return isReverseComplement_;
	}

	public void setIsReverseComplement(Boolean isReverseComplement) {
		isReverseComplement_ = isReverseComplement;
	}

	public Integer getStartPosition() {
		return startPosition_;
	}

	public void setStartPosition(Integer startPosition) {
		startPosition_ = startPosition;
	}

	public Integer getEndPosition() {
		return endPosition_;
	}

	public void setEndPosition(Integer endPosition) {
		endPosition_ = endPosition;
	}

	public String getLocationPattern() {
		return locationPattern_;
	}

	public void setLocationPattern(String locationPattern) {
		locationPattern_ = locationPattern;
	}

	public List<JoinPartItem> getJoinPartItems() {
		return joinPartItems_;
	}

	public Color getDisplayColor() {
		return displayColor_;
	}

	public void setDisplayColor(Color displayColor) {
		displayColor_ = displayColor;
	}

	@Override
	public String toString() {
		String identifier = qualifiers_.get(0);
		// TODO: do some string operations
		identifier += " " + locationPattern_;
		return identifier;
	}

	private String annotationName_ = "";
	private List<String> qualifiers_;
	private String partSequence_ = "";
	// private String partSequenceOriginal_ = "";
	private Boolean isReverseComplement_ = false;
	private Integer startPosition_ = 0;
	private Integer endPosition_ = 0;
	private String locationPattern_ = "";
	private List<JoinPartItem> joinPartItems_;
	private Color displayColor_ = null;

}
