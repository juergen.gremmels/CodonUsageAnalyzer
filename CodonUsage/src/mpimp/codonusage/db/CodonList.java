/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mpimp.codonusage.util.SequenceCalculator;

public class CodonList extends ArrayList<String> {

	private static final long serialVersionUID = 1L;

	public CodonList(String sequence) {
		super();
		sequence_ = sequence;
		splitSequenceIntoCodons();
	}

//	public CodonList(List<AnnotationRecord> annotationRecords) {
//		super();
//		sequence_ = "";
//		for (AnnotationRecord currentRecord : annotationRecords) {
//			sequence_ += currentRecord.getPartSequence();
//		}
//		splitSequenceIntoCodons();
//	}

	public List<Integer> getPositionsOfCodonGroupBaseCount(String codonGroup, Integer leftBoundary,
			Integer rightBoundary) {
		// TODO: handle dummy bases (N)
		List<Integer> bpIndices = new ArrayList<Integer>();
		String filteredCodonGroup = SequenceCalculator.trimStringToTripletString(codonGroup);
		Integer noOfTriplets = filteredCodonGroup.length() / 3;
		Integer baseIndex = -1;
		for (int index = 0; index < this.size(); index++) {
			if (index <= this.size() - noOfTriplets) {
				String matchString = "";
				for (int i = 0; i < noOfTriplets; i++) {
					matchString += this.get(index + i);
				}
				if (!matchString.equals("") && matches(matchString, filteredCodonGroup)) {
					baseIndex = index * 3 + 1;
					if (baseIndex >= leftBoundary && baseIndex <= rightBoundary) {
						bpIndices.add(baseIndex);
					}
				}
			}
		}
		return bpIndices;
	}

	public Integer getFrequencyOfCodon(String codon) {
		Integer counter = 0;
		for (String currentCodon : this) {
			if (currentCodon.equalsIgnoreCase(codon)) {
				counter++;
			}
		}
		return counter;
	}

	public Map<Integer, String> getPositionMapBaseCount() {
		Map<Integer, String> positionMap = new TreeMap<Integer, String>();
		Integer position = 1;
		for (String currentCodon : this) {
			positionMap.put(position, currentCodon);
			position += 3;
		}
		return positionMap;
	}

	public Map<Integer, String> getPositionMapCodonCount() {
		Map<Integer, String> positionMap = new TreeMap<Integer, String>();
		Integer position = 0;
		for (String currentCodon : this) {
			position++;
			positionMap.put(position, currentCodon);
		}
		return positionMap;
	}

	private Boolean matches(String stringToMatch, String searchPattern) {
		searchPattern = searchPattern.toUpperCase();
		stringToMatch = stringToMatch.toUpperCase();
		if (searchPattern.contains("N")) {
			searchPattern = buildRegExpForDummySearch(searchPattern);
			Pattern pattern = Pattern.compile(searchPattern);
			Matcher matcher = pattern.matcher(stringToMatch);
			return matcher.matches();
		} else {
			return stringToMatch.equals(searchPattern);
		}
	}

	private String buildRegExpForDummySearch(String searchPattern) {
		searchPattern = searchPattern.toUpperCase();
		String regExp = searchPattern.replace("N", "[AGCT]");
		return regExp;
	}

	private void splitSequenceIntoCodons() {
		if (sequence_ != null && !sequence_.equals("")) {
			sequence_ = sequence_.trim();
			sequence_ = sequence_.toUpperCase();
			String codon = "";
			for (int i = 0; i < sequence_.length() - 2; i += 3) {
				codon = sequence_.substring(i, i + 3);
				this.add(codon);
			}
		}
	}

	private String sequence_ = "";

}
