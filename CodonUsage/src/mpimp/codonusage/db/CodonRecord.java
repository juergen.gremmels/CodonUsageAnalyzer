/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.db;

import java.awt.Color;

public class CodonRecord {

	public String getAminoAcid() {
		return aminoAcid_;
	}
	public void setAminoAcid(String aminoAcid) {
		aminoAcid_ = aminoAcid;
	}
	public String getCodon() {
		return codon_;
	}
	public void setCodon(String codon) {
		codon_ = codon;
	}
	public Integer getPositionInPartSequence() {
		return positionInPartSequence_;
	}
	public void setPositionInPartSequence(Integer positionInPartSequence) {
		positionInPartSequence_ = positionInPartSequence;
	}
	public Integer getPositionInAnalysisSequence() {
		return positionInAnalysisSequence_;
	}
	public void setPositionInAnalysisSequence(Integer positionInAnalysisSequence) {
		positionInAnalysisSequence_ = positionInAnalysisSequence;
	}
	public Integer getPositionInGenome() {
		return positionInGenome_;
	}
	public void setPositionInGenome(Integer positionInGenome) {
		positionInGenome_ = positionInGenome;
	}
	public CodonUsageRecord getCodonUsageRecord() {
		return codonUsageRecord_;
	}
	public void setCodonUsageRecord(CodonUsageRecord codonUsageRecord) {
		codonUsageRecord_ = codonUsageRecord;
	}
	public String getAnnotationOfSourceSequence() {
		return annotationOfSourceSequence_;
	}
	public void setAnnotationOfSourceSequence(String annotationOfSourceSequence) {
		annotationOfSourceSequence_ = annotationOfSourceSequence;
	}
	public Boolean getFromReverseComplement() {
		return fromReverseComplement_;
	}
	public void setFromReverseComplement(Boolean fromReverseComplement) {
		fromReverseComplement_ = fromReverseComplement;
	}
	public Color getDisplayColor() {
		return displayColor_;
	}
	public void setDisplayColor(Color displayColor) {
		displayColor_ = displayColor;
	}
	private String aminoAcid_;
	private String codon_;
	private Integer positionInPartSequence_;
	private Integer positionInAnalysisSequence_;
	private Integer positionInGenome_;
	private CodonUsageRecord codonUsageRecord_;
	private String annotationOfSourceSequence_;
	private Boolean fromReverseComplement_ = false;
	private Color displayColor_ = null;
}
