/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.db;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import mpimp.codonusage.ui.CUXYSeries;
import mpimp.codonusage.ui.CUChart.ChartType;

public class CodonUsageModel {

	public CodonUsageModel() {
		searchPatternList_ = new ArrayList<String>();
		curCollectionForAnalysis_ = new HashMap<String, CodonUsageRecord>();
		positions_ = new HashMap<Integer, String>();
		searchPatternPositionsMap_ = new HashMap<String, List<Integer>>();
		loadedSequencesForAnalysis_ = new ArrayList<AnnotationRecord>();
		loadedSequencesAsReference_ = new ArrayList<AnnotationRecord>();
		sequencesForAnalysis_ = new ArrayList<AnnotationRecord>();
		sequencesAsReference_ = new ArrayList<AnnotationRecord>();
		curCollectionForAnalysis_ = new HashMap<String, CodonUsageRecord>();
		curCollectionAsReference_ = new HashMap<String, CodonUsageRecord>();
		codonCollectionForAnalysis_ = new TreeMap<Integer, CodonRecord>();
		codonCollectionAsReference_ = new TreeMap<Integer, CodonRecord>();
		codonCollectionForAnalysisPosInAnalysisSequence_ = new TreeMap<Integer, CodonRecord>();
		searchResultSeries_ = new ArrayList<CUXYSeries>();
		dataSeriesMap_ = new HashMap<String, CUXYSeries>();
		colorsForSearchPatternMap_ = new HashMap<String, Color>();
	}

	public String getSourceFilePath() {
		return sourceFilePath_;
	}

	public void setSourceFilePath(String sourceFilePath) {
		sourceFilePath_ = sourceFilePath;
	}

	public String getTableFilePath() {
		return tableFilePath_;
	}

	public void setTableFilePath(String tableFilePath) {
		tableFilePath_ = tableFilePath;
	}

	public String getSearchResultFilePath() {
		return searchResultFilePath_;
	}

	public void setSearchResultFilePath(String searchResultFilePath) {
		searchResultFilePath_ = searchResultFilePath;
	}

	public String getLastOpenedDirPath() {
		return lastOpenedDirPath_;
	}

	public void setLastOpenedDirPath(String lastOpenedDirPath) {
		lastOpenedDirPath_ = lastOpenedDirPath;
	}

	public String getWholeGenomeSequenceForAnalysis() {
		return wholeGenomeSequenceForAnalyis_;
	}

	public void setWholeGenomeSequenceForAnalysis(String wholeGenomeSequence) {
		wholeGenomeSequenceForAnalyis_ = wholeGenomeSequence;
	}

	public String getWholeGenomeSequenceAsReference() {
		return wholeGenomeSequenceAsReference_;
	}

	public void setWholeGenomeSequenceAsReference(String wholeGenomeSequenceAsReference) {
		wholeGenomeSequenceAsReference_ = wholeGenomeSequenceAsReference;
	}

	public String getSequenceForAnalysis() {
		return sequenceForAnalysis_;
	}

	public void setSequenceForAnalysis(String sequenceForCalculation) {
		sequenceForAnalysis_ = sequenceForCalculation;
	}

	public String getSequenceAsReference() {
		return sequenceAsReference_;
	}

	public void setSequenceAsReference(String sequenceAsReference) {
		sequenceAsReference_ = sequenceAsReference;
	}

	public List<AnnotationRecord> getLoadedSequencesForAnalysis() {
		return loadedSequencesForAnalysis_;
	}

	public List<AnnotationRecord> getLoadedSequencesAsReference() {
		return loadedSequencesAsReference_;
	}

	public String getSearchString() {
		return searchString_;
	}

	public void setSearchString(String searchString) {
		searchString_ = searchString;
	}

	public List<String> getSearchPatternList() {
		return searchPatternList_;
	}

	public void setSearchPatternList(List<String> searchPatternList) {
		searchPatternList_ = searchPatternList;
	}

	public Map<String, CodonUsageRecord> getCurCollectionForAnalysis() {
		return curCollectionForAnalysis_;
	}

	public void setCurCollectionForAnalysis(Map<String, CodonUsageRecord> curCollection) {
		curCollectionForAnalysis_ = curCollection;
	}

	public Map<String, CodonUsageRecord> getCurCollectionAsReference() {
		return curCollectionAsReference_;
	}

	public void setCurCollectionAsReference(Map<String, CodonUsageRecord> curCollectionAsReference) {
		curCollectionAsReference_ = curCollectionAsReference;
	}

	public Map<Integer, CodonRecord> getCodonCollectionForAnalysis() {
		return codonCollectionForAnalysis_;
	}

	public Map<Integer, CodonRecord> getCodonCollectionAsReference() {
		return codonCollectionAsReference_;
	}

	public void setCodonCollectionAsReference(Map<Integer, CodonRecord> codonCollectionAsReference) {
		codonCollectionAsReference_ = codonCollectionAsReference;
	}

	public Map<Integer, CodonRecord> getCodonCollectionForAnalysisPosInAnalysisSequence() {
		return codonCollectionForAnalysisPosInAnalysisSequence_;
	}

	public void setCodonCollectionForAnalysisPosInAnalysisSequence(
			Map<Integer, CodonRecord> codonCollectionForAnalysisPosInAnalysisSequence) {
		codonCollectionForAnalysisPosInAnalysisSequence_ = codonCollectionForAnalysisPosInAnalysisSequence;
	}

	public Map<Integer, String> getPositions() {
		return positions_;
	}

	public void setPositions(Map<Integer, String> positions) {
		positions_ = positions;
	}

	public Map<String, List<Integer>> getSearchPatternPositionsMap() {
		return searchPatternPositionsMap_;
	}

	public void setSearchPatternPositionsMap(Map<String, List<Integer>> searchPatternPositionsMap) {
		searchPatternPositionsMap_ = searchPatternPositionsMap;
	}

	public Double getCodonAdaptationIndex() {
		return codonAdaptationIndex_;
	}

	public void setCodonAdaptationIndex(Double codonAdaptationIndex) {
		codonAdaptationIndex_ = codonAdaptationIndex;
	}

	public List<AnnotationRecord> getSequencesForAnalysis() {
		return sequencesForAnalysis_;
	}

	public void setSequencesForAnalysis(List<AnnotationRecord> sequencesForAnalysis) {
		sequencesForAnalysis_ = sequencesForAnalysis;
	}

	public List<AnnotationRecord> getSequencesAsReference() {
		return sequencesAsReference_;
	}

	public void setSequencesAsReference(List<AnnotationRecord> sequencesAsReference) {
		sequencesAsReference_ = sequencesAsReference;
	}

	public Boolean getUseTripletFrame() {
		return useTripletFrame_;
	}

	public void setUseTripletFrame(Boolean useTripletFrame) {
		useTripletFrame_ = useTripletFrame;
	}

	public Integer getPositionCounterForFreeTextSequence() {
		return positionCounterForFreeTextSequence_;
	}

	public void setPositionCounterForFreeTextSequence(Integer positionCounterForFreeTextSequence) {
		positionCounterForFreeTextSequence_ = positionCounterForFreeTextSequence;
	}

	public Integer getPositionCounterForFreeTextSequenceAsReference() {
		return positionCounterForFreeTextSequenceAsReference_;
	}

	public void setPositionCounterForFreeTextSequenceAsReference(Integer positionCounterForFreeTextSequenceAsReference) {
		positionCounterForFreeTextSequenceAsReference_ = positionCounterForFreeTextSequenceAsReference;
	}

	public Integer getLeftBoundary() {
		return leftBoundary_;
	}

	public void setLeftBoundary(Integer leftBoundary) {
		leftBoundary_ = leftBoundary;
	}

	public Integer getRightBoundary() {
		return rightBoundary_;
	}

	public void setRightBoundary(Integer rightBoundary) {
		rightBoundary_ = rightBoundary;
	}

	public CodonRecord getSelectedCodonRecord() {
		return selectedCodonRecord_;
	}

	public void setSelectedCodonRecord(CodonRecord selectedCodonRecord) {
		selectedCodonRecord_ = selectedCodonRecord;
	}

	public List<String> getSelectedCodonsForExport() {
		return selectedCodonsForExport_;
	}

	public void setSelectedCodonsForExport(List<String> selectedCodonsForExport) {
		selectedCodonsForExport_ = selectedCodonsForExport;
	}

	public ChartType getChartType() {
		return chartType_;
	}

	public void setChartType(ChartType chartType) {
		chartType_ = chartType;
	}

	public List<CUXYSeries> getSearchResultSeries() {
		return searchResultSeries_;
	}

	public Map<String, CUXYSeries> getDataSeriesMap() {
		return dataSeriesMap_;
	}

	public Map<String, Color> getColorsForSearchPatternMap() {
		return colorsForSearchPatternMap_;
	}

	public void resetDataSource() {
		sourceFilePath_ = "";
		loadedSequencesForAnalysis_ = new ArrayList<AnnotationRecord>();
		loadedSequencesAsReference_ = new ArrayList<AnnotationRecord>();
		wholeGenomeSequenceForAnalyis_ = "";
	}
	
	public void resetLoadedRawData() {
		sequencesForAnalysis_ = new ArrayList<AnnotationRecord>();
		sequencesAsReference_ = new ArrayList<AnnotationRecord>();
	}
	
	public void resetCalculationData() {
		curCollectionForAnalysis_ = new HashMap<String, CodonUsageRecord>();
		positions_ = new HashMap<Integer, String>();
		curCollectionForAnalysis_ = new HashMap<String, CodonUsageRecord>();
		curCollectionAsReference_ = new HashMap<String, CodonUsageRecord>();
		codonCollectionForAnalysis_ = new TreeMap<Integer, CodonRecord>();
		codonCollectionForAnalysisPosInAnalysisSequence_ = new TreeMap<Integer, CodonRecord>();
		codonCollectionAsReference_ = new TreeMap<Integer, CodonRecord>();
		positionCounterForFreeTextSequence_ = 1;
		positionCounterForFreeTextSequenceAsReference_ = 1;
		sequenceForAnalysis_ = "";
		sequenceAsReference_ = "";
		codonAdaptationIndex_ = 0.0;
		tableFilePath_ = "";
		searchResultFilePath_ = "";
		selectedCodonRecord_ = null;
		useTripletFrame_ = true;
		leftBoundary_ = -1;
		rightBoundary_ = -1;
		searchPatternList_ = new ArrayList<String>();
		searchString_ = "";
		searchPatternPositionsMap_ = new HashMap<String, List<Integer>>();
		searchResultSeries_ = new ArrayList<CUXYSeries>();
		dataSeriesMap_ = new HashMap<String, CUXYSeries>();
		colorsForSearchPatternMap_ = new HashMap<String, Color>();
	}
	
	//currently not used - but maybe later
//	public void resetAllData() {
//		resetDataSource();
//		resetLoadedRawData();
//		resetCalculationData();
//	}
	
	private String sourceFilePath_ = "";
	private String tableFilePath_ = "";
	private String searchResultFilePath_ = "";
	private String lastOpenedDirPath_ = "";
	
	private Boolean useTripletFrame_ = true;

	private String wholeGenomeSequenceForAnalyis_ = "";
	private String wholeGenomeSequenceAsReference_ = "";
	private String sequenceForAnalysis_ = "";
	private String sequenceAsReference_ = "";
	
	private List<AnnotationRecord> loadedSequencesForAnalysis_;
	private List<AnnotationRecord> loadedSequencesAsReference_;
	private List<AnnotationRecord> sequencesForAnalysis_;
	private List<AnnotationRecord> sequencesAsReference_;

	private String searchString_ = "";
	private List<String> searchPatternList_;

	private Map<String, CodonUsageRecord> curCollectionForAnalysis_;
	private Map<String, CodonUsageRecord> curCollectionAsReference_;
	private Map<Integer, CodonRecord> codonCollectionForAnalysis_;
	private Map<Integer, CodonRecord> codonCollectionForAnalysisPosInAnalysisSequence_;
	private Map<Integer, CodonRecord> codonCollectionAsReference_;
	private Map<Integer, String> positions_;
	private Map<String, List<Integer>> searchPatternPositionsMap_;

	private Double codonAdaptationIndex_;
	
	private Integer positionCounterForFreeTextSequence_ = 1;
	private Integer positionCounterForFreeTextSequenceAsReference_ = 1;
	
	private Integer leftBoundary_ = -1;
	private Integer rightBoundary_ = -1;
	
	private CodonRecord selectedCodonRecord_ = null;
	
	private List<String> selectedCodonsForExport_;
	
	private ChartType chartType_ = ChartType.SCATTER_PLOT;
	
	private List<CUXYSeries> searchResultSeries_;
	private Map<String, CUXYSeries> dataSeriesMap_;
	private Map<String, Color> colorsForSearchPatternMap_;
}
