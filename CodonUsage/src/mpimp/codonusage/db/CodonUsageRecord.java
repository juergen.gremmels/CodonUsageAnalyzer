/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.db;

public class CodonUsageRecord {

	public CodonUsageRecord() {
		
	}
	
	public CodonUsageRecord(String aminoAcid, String codon, Integer frequency) {
		aminoAcid_ = aminoAcid;
		codon_ = codon;
		frequency_ = frequency;
	}
	
	public String getAminoAcid() {
		return aminoAcid_;
	}
	public void setAminoAcid(String aminoAcid) {
		aminoAcid_ = aminoAcid;
	}
	public String getCodon() {
		return codon_;
	}
	public void setCodon(String codon) {
		codon_ = codon;
	}
	public Integer getFrequency() {
		return frequency_;
	}
	public void setFrequency(Integer frequency) {
		frequency_ = frequency;
	}
	public Double getFractionRelToAnalysisSequence() {
		return fractionRelToAnalysisSequence_;
	}
	public void setFractionRelToAnalysisSequence(Double fraction) {
		fractionRelToAnalysisSequence_ = fraction;
	}
	
	public Double getFractionRelToReferenceSequence() {
		return fractionRelToReferenceSequence_;
	}

	public void setFractionRelToReferenceSequence(Double fractionRelToReferenceSequence) {
		fractionRelToReferenceSequence_ = fractionRelToReferenceSequence;
	}

	public Double getAdaptivenessRelToAnalysisSequence() {
		return adaptivenessRelToAnalysisSequence_;
	}

	public void setAdaptivenessRelToAnalysisSequence(Double relativeAdaptiveness) {
		adaptivenessRelToAnalysisSequence_ = relativeAdaptiveness;
	}

	public Double getAdaptivenessRelToReferenceSequence() {
		return adaptivenessRelToReferenceSequence_;
	}

	public void setAdaptivenessRelToReferenceSequence(Double adaptivenessRelToReferenceSequence) {
		adaptivenessRelToReferenceSequence_ = adaptivenessRelToReferenceSequence;
	}

	public Double getAdaptivenessRelToAnalysisSequenceNorm() {
		return adaptivenessRelToAnalysisSequenceNorm_;
	}

	public void setAdaptivenessRelToAnalysisSequenceNorm(Double adaptivenessRelToAnalysisSequenceNorm) {
		adaptivenessRelToAnalysisSequenceNorm_ = adaptivenessRelToAnalysisSequenceNorm;
	}

	public Double getAdaptivenessRelToReferenceSequenceNorm() {
		return adaptivenessRelToReferenceSequenceNorm_;
	}

	public void setAdaptivenessRelToReferenceSequenceNorm(Double adaptivenessRelToReferenceSequenceNorm) {
		adaptivenessRelToReferenceSequenceNorm_ = adaptivenessRelToReferenceSequenceNorm;
	}

	public Integer getPositionInReference() {
		return positionInReference_;
	}

	public void setPositionInReference(Integer positionInReference) {
		positionInReference_ = positionInReference;
	}

	public Integer getPositionInAnalysis() {
		return positionInAnalysis_;
	}

	public void setPositionInAnalysis(Integer positionInAnalysis) {
		positionInAnalysis_ = positionInAnalysis;
	}

	private String aminoAcid_;
	private String codon_;
	private Integer frequency_;
	private Double fractionRelToAnalysisSequence_;
	private Double fractionRelToReferenceSequence_;
	private Double adaptivenessRelToAnalysisSequence_;
	private Double adaptivenessRelToReferenceSequence_;
	private Double adaptivenessRelToAnalysisSequenceNorm_;
	private Double adaptivenessRelToReferenceSequenceNorm_;
	private Integer positionInReference_;
	private Integer positionInAnalysis_;
	
}
