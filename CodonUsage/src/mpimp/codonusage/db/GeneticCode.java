/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeneticCode {

	private static Map<String, List<String>> aminoAcidCodonMap_;
	private static Map<String, String> codonAminoAcidMap_;

	static {
		aminoAcidCodonMap_ = new HashMap<String, List<String>>();
		aminoAcidCodonMap_.put(">", new ArrayList<String>());
		aminoAcidCodonMap_.get(">").add("ATG");
		aminoAcidCodonMap_.put("M", new ArrayList<String>());
		aminoAcidCodonMap_.get("M").add("ATG");
		aminoAcidCodonMap_.put("W", new ArrayList<String>());
		aminoAcidCodonMap_.get("W").add("TGG");
		aminoAcidCodonMap_.put("T", new ArrayList<String>());
		aminoAcidCodonMap_.get("T").add("(TGA)");
		aminoAcidCodonMap_.put("O", new ArrayList<String>());
		aminoAcidCodonMap_.get("O").add("(TAG)");
		aminoAcidCodonMap_.put("Y", new ArrayList<String>());
		aminoAcidCodonMap_.get("Y").add("TAT");
		aminoAcidCodonMap_.get("Y").add("TAC");
		aminoAcidCodonMap_.put("F", new ArrayList<String>());
		aminoAcidCodonMap_.get("F").add("TTT");
		aminoAcidCodonMap_.get("F").add("TTC");
		aminoAcidCodonMap_.put("C", new ArrayList<String>());
		aminoAcidCodonMap_.get("C").add("TGT");
		aminoAcidCodonMap_.get("C").add("TGC");
		aminoAcidCodonMap_.put("N", new ArrayList<String>());
		aminoAcidCodonMap_.get("N").add("AAT");
		aminoAcidCodonMap_.get("N").add("AAC");
		aminoAcidCodonMap_.put("D", new ArrayList<String>());
		aminoAcidCodonMap_.get("D").add("GAT");
		aminoAcidCodonMap_.get("D").add("GAC");
		aminoAcidCodonMap_.put("Q", new ArrayList<String>());
		aminoAcidCodonMap_.get("Q").add("CAA");
		aminoAcidCodonMap_.get("Q").add("CAG");
		aminoAcidCodonMap_.put("E", new ArrayList<String>());
		aminoAcidCodonMap_.get("E").add("GAA");
		aminoAcidCodonMap_.get("E").add("GAG");
		aminoAcidCodonMap_.put("H", new ArrayList<String>());
		aminoAcidCodonMap_.get("H").add("CAT");
		aminoAcidCodonMap_.get("H").add("CAC");
		aminoAcidCodonMap_.put("K", new ArrayList<String>());
		aminoAcidCodonMap_.get("K").add("AAA");
		aminoAcidCodonMap_.get("K").add("AAG");
		aminoAcidCodonMap_.put("I", new ArrayList<String>());
		aminoAcidCodonMap_.get("I").add("ATT");
		aminoAcidCodonMap_.get("I").add("ATC");
		aminoAcidCodonMap_.get("I").add("ATA");
		aminoAcidCodonMap_.put("G", new ArrayList<String>());
		aminoAcidCodonMap_.get("G").add("GGT");
		aminoAcidCodonMap_.get("G").add("GGC");
		aminoAcidCodonMap_.get("G").add("GGA");
		aminoAcidCodonMap_.get("G").add("GGG");
		aminoAcidCodonMap_.put("A", new ArrayList<String>());
		aminoAcidCodonMap_.get("A").add("GCT");
		aminoAcidCodonMap_.get("A").add("GCC");
		aminoAcidCodonMap_.get("A").add("GCA");
		aminoAcidCodonMap_.get("A").add("GCG");
		aminoAcidCodonMap_.put("V", new ArrayList<String>());
		aminoAcidCodonMap_.get("V").add("GTT");
		aminoAcidCodonMap_.get("V").add("GTC");
		aminoAcidCodonMap_.get("V").add("GTA");
		aminoAcidCodonMap_.get("V").add("GTG");
		aminoAcidCodonMap_.put("T", new ArrayList<String>());
		aminoAcidCodonMap_.get("T").add("ACT");
		aminoAcidCodonMap_.get("T").add("ACC");
		aminoAcidCodonMap_.get("T").add("ACA");
		aminoAcidCodonMap_.get("T").add("ACG");
		aminoAcidCodonMap_.put("P", new ArrayList<String>());
		aminoAcidCodonMap_.get("P").add("CCT");
		aminoAcidCodonMap_.get("P").add("CCC");
		aminoAcidCodonMap_.get("P").add("CCA");
		aminoAcidCodonMap_.get("P").add("CCG");
		aminoAcidCodonMap_.put("L", new ArrayList<String>());
		aminoAcidCodonMap_.get("L").add("CTT");
		aminoAcidCodonMap_.get("L").add("CTC");
		aminoAcidCodonMap_.get("L").add("CTA");
		aminoAcidCodonMap_.get("L").add("CTG");
		aminoAcidCodonMap_.get("L").add("TTA");
		aminoAcidCodonMap_.get("L").add("TTG");
		aminoAcidCodonMap_.put("S", new ArrayList<String>());
		aminoAcidCodonMap_.get("S").add("TCT");
		aminoAcidCodonMap_.get("S").add("TCC");
		aminoAcidCodonMap_.get("S").add("TCA");
		aminoAcidCodonMap_.get("S").add("TCG");
		aminoAcidCodonMap_.get("S").add("AGT");
		aminoAcidCodonMap_.get("S").add("AGC");
		aminoAcidCodonMap_.put("R", new ArrayList<String>());
		aminoAcidCodonMap_.get("R").add("CGT");
		aminoAcidCodonMap_.get("R").add("CGC");
		aminoAcidCodonMap_.get("R").add("CGA");
		aminoAcidCodonMap_.get("R").add("CGG");
		aminoAcidCodonMap_.get("R").add("AGA");
		aminoAcidCodonMap_.get("R").add("AGG");
		aminoAcidCodonMap_.put("<", new ArrayList<String>());
		aminoAcidCodonMap_.get("<").add("TAA");
		aminoAcidCodonMap_.get("<").add("TAG");
		aminoAcidCodonMap_.get("<").add("TGA");

		codonAminoAcidMap_ = new HashMap<String, String>();

		codonAminoAcidMap_.put("ATG", ">");
		codonAminoAcidMap_.put("ATG", "M");
		codonAminoAcidMap_.put("TGG", "W");
		codonAminoAcidMap_.put("(TGA)", "T");
		codonAminoAcidMap_.put("(TAG)", "O");
		codonAminoAcidMap_.put("TAT", "Y");
		codonAminoAcidMap_.put("TAC", "Y");
		codonAminoAcidMap_.put("TTT", "F");
		codonAminoAcidMap_.put("TTC", "F");
		codonAminoAcidMap_.put("TGT", "C");
		codonAminoAcidMap_.put("TGC", "C");
		codonAminoAcidMap_.put("AAT", "N");
		codonAminoAcidMap_.put("AAC", "N");
		codonAminoAcidMap_.put("GAT", "D");
		codonAminoAcidMap_.put("GAC", "D");
		codonAminoAcidMap_.put("CAA", "Q");
		codonAminoAcidMap_.put("CAG", "Q");
		codonAminoAcidMap_.put("GAA", "E");
		codonAminoAcidMap_.put("GAG", "E");
		codonAminoAcidMap_.put("CAT", "H");
		codonAminoAcidMap_.put("CAC", "H");
		codonAminoAcidMap_.put("AAA", "K");
		codonAminoAcidMap_.put("AAG", "K");
		codonAminoAcidMap_.put("ATT", "I");
		codonAminoAcidMap_.put("ATC", "I");
		codonAminoAcidMap_.put("ATA", "I");
		codonAminoAcidMap_.put("GGT", "G");
		codonAminoAcidMap_.put("GGC", "G");
		codonAminoAcidMap_.put("GGA", "G");
		codonAminoAcidMap_.put("GGG", "G");
		codonAminoAcidMap_.put("GCT", "A");
		codonAminoAcidMap_.put("GCC", "A");
		codonAminoAcidMap_.put("GCA", "A");
		codonAminoAcidMap_.put("GCG", "A");
		codonAminoAcidMap_.put("GTT", "V");
		codonAminoAcidMap_.put("GTC", "V");
		codonAminoAcidMap_.put("GTA", "V");
		codonAminoAcidMap_.put("GTG", "V");
		codonAminoAcidMap_.put("ACT", "T");
		codonAminoAcidMap_.put("ACC", "T");
		codonAminoAcidMap_.put("ACA", "T");
		codonAminoAcidMap_.put("ACG", "T");
		codonAminoAcidMap_.put("CCT", "P");
		codonAminoAcidMap_.put("CCC", "P");
		codonAminoAcidMap_.put("CCA", "P");
		codonAminoAcidMap_.put("CCG", "P");
		codonAminoAcidMap_.put("CTT", "L");
		codonAminoAcidMap_.put("CTC", "L");
		codonAminoAcidMap_.put("CTA", "L");
		codonAminoAcidMap_.put("CTG", "L");
		codonAminoAcidMap_.put("TTA", "L");
		codonAminoAcidMap_.put("TTG", "L");
		codonAminoAcidMap_.put("TCT", "S");
		codonAminoAcidMap_.put("TCC", "S");
		codonAminoAcidMap_.put("TCA", "S");
		codonAminoAcidMap_.put("TCG", "S");
		codonAminoAcidMap_.put("AGT", "S");
		codonAminoAcidMap_.put("AGC", "S");
		codonAminoAcidMap_.put("CGT", "R");
		codonAminoAcidMap_.put("CGC", "R");
		codonAminoAcidMap_.put("CGA", "R");
		codonAminoAcidMap_.put("CGG", "R");
		codonAminoAcidMap_.put("AGA", "R");
		codonAminoAcidMap_.put("AGG", "R");
		codonAminoAcidMap_.put("TAA", "<");
		codonAminoAcidMap_.put("TAG", "<");
		codonAminoAcidMap_.put("TGA", "<");

	}

	public static Map<String, List<String>> getAminoAcidCodonMap() {
		return aminoAcidCodonMap_;
	}

	public static Map<String, String> getCodonAminoAcidMap() {
		return codonAminoAcidMap_;
	}

}
