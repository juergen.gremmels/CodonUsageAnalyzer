/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.db;

public class JoinPartItem {

	public Integer getStartPosition() {
		return startPosition_;
	}
	public void setStartPosition(Integer startPosition) {
		startPosition_ = startPosition;
	}
	public Integer getEndPosition() {
		return endPosition_;
	}
	public void setEndPosition(Integer endPosition) {
		endPosition_ = endPosition;
	}
	public String getSequence() {
		return sequence_;
	}
	public void setSequence(String sequence) {
		sequence_ = sequence;
	}
	public String getSequenceOriginal() {
		return sequenceOriginal_;
	}
	public void setSequenceOriginal(String sequenceOriginal) {
		sequenceOriginal_ = sequenceOriginal;
	}
	public Boolean getIsReverseComplement() {
		return isReverseComplement_;
	}
	public void setIsReverseComplement(Boolean isReverseComplement) {
		isReverseComplement_ = isReverseComplement;
	}
	private Integer startPosition_ = -1;
	private Integer endPosition_ = -1;
	private String sequence_ = "";
	private String sequenceOriginal_ = "";
	private Boolean isReverseComplement_ = false;

}
