/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.teststuff;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import mpimp.codonusage.db.AnnotationRecord;
import mpimp.codonusage.db.JoinPartItem;
import mpimp.codonusage.util.GenbankAnnotationParser;
import mpimp.codonusage.util.JoinPartItemComparator;

public class DataCollector {

	public static void main(String[] args) throws Exception {
		//String genbankFilePath = "H:\\Programmierung_BioInf\\CodonUsage\\RawData\\Chloroplast_genomes\\Nicotiana_tabacum_chloroplast_genome_DNA.gb";
		//String genbankFilePath = "H:\\Programmierung_BioInf\\CodonUsage\\RawData\\Chloroplast_genomes\\Arabidopsis_chloroplast_Columbia_testFile3.gb";
		 String genbankFilePath =
		 "H:\\Programmierung_BioInf\\CodonUsage\\GenbankTest.gb";

		// String outputFilePath =
		// "H:\\Programmierung_BioInf\\CodonUsage\\tmp\\Arabidopsis_chloroplast_Columbia.txt";
		//String outputFilePath = "H:\\Programmierung_BioInf\\CodonUsage\\tmp\\Nicotiana_tabacum_chloroplast_genome_DNA_sequences.txt";
		//String outputFilePath = "H:\\Programmierung_BioInf\\CodonUsage\\tmp\\Arabidopsis_chloroplast_Columbia_testFile3_sequences.txt";
		 String outputFilePath = "H:\\Programmierung_BioInf\\CodonUsage\\GenbankTest_sequences.txt";
		Path outputPath = Paths.get(outputFilePath);

		List<AnnotationRecord> annotationRecords = null;
		List<String> outputFileContent = new ArrayList<String>();
		//
		// Map<String, String> annotationMap =
		// AnnotationMapCreator.createAnnotationMap();

		try {
			annotationRecords = GenbankAnnotationParser.parseAnnotations(genbankFilePath);

			for (AnnotationRecord currentRecord : annotationRecords) {

				System.out.println(currentRecord.toString());
				System.out.println(currentRecord.getPartSequence());
				//System.out.println(currentRecord.getPartSequenceOriginal());
				System.out.println();
				
				outputFileContent.add(currentRecord.toString());
				outputFileContent.add(currentRecord.getPartSequence());
				
				List<JoinPartItem> joinPartItems = currentRecord.getJoinPartItems();
				Collections.sort(joinPartItems, new JoinPartItemComparator());
				for (JoinPartItem jpi : joinPartItems) {
					String sequence = jpi.getSequence();
					sequence = sequence.trim();
					sequence = sequence.toUpperCase();
					
					Integer rest =sequence.length() % 3; 
					
					if (rest == 0) {
						outputFileContent.add("splicing at codon boundaries " +  rest);
					} else {
						outputFileContent.add("splicing within codon " + rest);
					}
					
				}
				
//				outputFileContent.add(currentRecord.getPartSequence());
//				outputFileContent.add(currentRecord.getPartSequenceOriginal());
				outputFileContent.add("");
				
				// System.out.println(currentRecord.getAnnotationName());
				// for (String qualifier : currentRecord.getQualifiers()) {
				// System.out.println(qualifier);
				// }
				// System.out.println(currentRecord.getPartSequence());
				// if
				// (currentRecord.getAnnotationName().equalsIgnoreCase("CDS")) {
				// String partSequence = currentRecord.getPartSequence();
				// String outputLine = "";
				// for (String qualifier : currentRecord.getQualifiers()) {
				// if (qualifier.startsWith("/gene=")) {
				// String geneCode = qualifier.substring(qualifier.indexOf("=")
				// + 1);
				// geneCode = geneCode.replace("\"", "");
				// geneCode = geneCode.toUpperCase();
				// String annotation = annotationMap.get(geneCode);
				// if (annotation != null) {
				// outputLine += annotation;
				// } else {
				// outputLine += geneCode;
				// System.out.println(geneCode);
				// }
				// break;
				// }
				// }
				// outputLine += " ";
				// outputLine += partSequence;
				//
				// outputFileContent.add(outputLine);
				// }
			}

			Files.write(outputPath, outputFileContent);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
