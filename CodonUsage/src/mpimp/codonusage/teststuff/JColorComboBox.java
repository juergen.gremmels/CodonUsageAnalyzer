/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.teststuff;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.swing.*;

/**
 *
 * @author sharath
 */
public class JColorComboBox extends JComboBox {

	static Hashtable<String, Color> colors;

	public JColorComboBox() {
		super();
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		Enumeration colorNames = addColors().keys();
		while (colorNames.hasMoreElements()) {
			String temp = colorNames.nextElement().toString();
			model.addElement(temp);
			System.out.println("colors" + temp);
		}
		setModel(model);
		setRenderer(new ColorRenderer(colors));
		this.setOpaque(true);
		this.setSelectedIndex(0);
	}

	@Override
	public void setSelectedItem(Object anObject) {
		super.setSelectedItem(anObject);

		setBackground((Color) colors.get(anObject));
		setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
		if (anObject.toString().equals("BLACK") || anObject.toString().equals("DARK_GRAY")) {
			setForeground(Color.white);
		}
	}

	public Color getSelectedColor() {

		return this.getBackground();
	}

	private Hashtable addColors() {

		colors = new Hashtable<String, Color>();

		colors.put("WHITE", Color.WHITE);
		colors.put("BLUE", Color.BLUE);
		colors.put("GREEN", Color.GREEN);
		colors.put("YELLOW", Color.YELLOW);
		colors.put("ORANGE", Color.ORANGE);
		colors.put("CYAN", Color.CYAN);
		colors.put("DARK_GRAY", Color.DARK_GRAY);
		colors.put("GRAY", Color.GRAY);
		colors.put("RED", Color.RED);
		colors.put("PINK", Color.PINK);
		colors.put("MAGENTA", Color.MAGENTA);
		colors.put("BLACK", Color.BLACK);

		return colors;
	}
}
