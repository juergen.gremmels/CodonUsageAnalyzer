/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.teststuff;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mpimp.codonusage.db.AnnotationRecord;
import mpimp.codonusage.util.SequenceCalculator;
import mpimp.codonusage.util.SequenceFileParser;

public class RegExpTester {

	public static void main(String[] args) throws IOException {
		String filePath = "H:\\Programmierung_BioInf\\CodonUsage\\RawData\\Chloroplast genomes\\Arabidopsis_chloroplast_Columbia_testFile2.gb";

		BufferedReader br = new BufferedReader(new FileReader(filePath));

		Pattern annotationNameLinePattern = Pattern.compile("^( {5})([a-zA-Z]{1,15})( +)([a-z0-9\\(\\)\\.,]+)");

		Pattern regionPattern = Pattern.compile("([0-9]+)\\.\\.([0-9]+)");

		Pattern specialRegionPattern = Pattern.compile("([a-z]+)\\(([a-z0-9.,\\(\\)]+)\\)");

		// String testString = " CDS join(74841..74846,75651..76292)";
		String group = "";
		// Matcher joinMatcher = joinPattern.matcher(testString);

		// while (joinMatcher.find()) {
		// group = joinMatcher.group(0);
		// Matcher regionMatcher = regionPattern.matcher(group);
		// while (regionMatcher.find()) {
		// for (int i = 1; i <= regionMatcher.groupCount(); i++) {
		// System.out.println(regionMatcher.group(i));
		// }
		// }
		// }

		String line = "";
		String rawSequence = SequenceFileParser.extractSequenceFromGenebankFile(filePath);
		rawSequence = rawSequence.toUpperCase();

		while ((line = br.readLine()) != null) {
			Matcher annotationNameLineMatcher = annotationNameLinePattern.matcher(line);

			if (annotationNameLineMatcher.matches()) {
				// System.out.println(annotationNameLineMatcher.group());
				if (annotationNameLineMatcher.group(2).equalsIgnoreCase("CDS")) {
					analyseRegion(rawSequence, "", annotationNameLineMatcher.group(4),
							new AnnotationRecord(annotationNameLineMatcher.group(2), false));
					System.out.println();
					System.out.println();
				}
				// System.out.println(annotationNameLineMatcher.group());
				// group = annotationNameLineMatcher.group(4);
				// Matcher regionMatcher = regionPattern.matcher(group);
				// if (regionMatcher.matches()) {
				// System.out.println("Simple region: " + regionMatcher.group(1)
				// + " " + regionMatcher.group(2));
				// }
				// Matcher specialRegionMatcher =
				// specialRegionPattern.matcher(group);
				// if (specialRegionMatcher.matches()) {
				// System.out.println(
				// "Complex region: " + specialRegionMatcher.group(1) + " " +
				// specialRegionMatcher.group(2));
				// }
				// System.out.println();
			}

		}
		br.close();

	}

	public static String extractPartSequence(String rawSequence, Integer begin, Integer end) {
		if (end > rawSequence.length()) {
			return rawSequence.substring(begin - 1);
		}
		return rawSequence.substring(begin - 1, end);
	}

	public static void analyseRegion(String rawSequence, String regionSpecifier, String region,
			AnnotationRecord annotationRecord) {
		System.out.println(regionSpecifier + " " + region);
		Pattern specialRegionPattern = Pattern.compile("(complement|join)\\(([a-z0-9.,\\(\\)]+)\\)");
		Pattern simpleRegionPattern = Pattern.compile("([0-9]+)\\.\\.([0-9]+)");
		Pattern joinPattern = Pattern.compile(
				"((complement|join)?\\(?[0-9]+\\.\\.[0-9]+\\)?)(,(complement|join)?\\(?([0-9]+\\.\\.[0-9]+\\)?))+");

		Matcher specialRegionMatcher = specialRegionPattern.matcher(region);
		Matcher simpleRegionMatcher = simpleRegionPattern.matcher(region);
		Matcher joinRegionMatcher = joinPattern.matcher(region);

		if (specialRegionMatcher.matches()) { // join(....) or complement(....)
			if (regionSpecifier.equalsIgnoreCase("complement")) {
				annotationRecord.setIsReverseComplement(true);
			}
			analyseRegion(rawSequence, specialRegionMatcher.group(1), specialRegionMatcher.group(2), annotationRecord);
		} else if (regionSpecifier.equalsIgnoreCase("join")) {
			if (joinRegionMatcher.matches()) {
				String[] joinParts = joinRegionMatcher.group(0).split(",");
				if (annotationRecord.getIsReverseComplement() == true) {
					for (int i = joinParts.length - 1; i >= 0; i--) {
						analyseRegion(rawSequence, "", joinParts[i], annotationRecord);
					}
				} else {
					for (int i = 0; i < joinParts.length; i++) {
						analyseRegion(rawSequence, "", joinParts[i], annotationRecord);
					}
				}
			}
		} else if (simpleRegionMatcher.matches()) { // 12345..12345
			String partSequence = annotationRecord.getPartSequence();
			String newPartSequence = extractPartSequence(rawSequence, new Integer(simpleRegionMatcher.group(1)),
					new Integer(simpleRegionMatcher.group(2)));
			System.out.println("New part sequence: " + newPartSequence);
			if (regionSpecifier.equalsIgnoreCase("complement") || annotationRecord.getIsReverseComplement() == true) {
				newPartSequence = SequenceCalculator.calculateReverseComplement(newPartSequence);
			}
			System.out.println("New part sequence (maybe complement): " + newPartSequence);
			partSequence += newPartSequence;
			System.out.println(partSequence);
			annotationRecord.setPartSequence(partSequence);
		}
	}

}
