/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.ui;

import java.awt.Color;
import java.awt.Paint;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.chart.renderer.xy.XYBarRenderer;

import mpimp.codonusage.db.CodonMapKey;
import mpimp.codonusage.db.CodonRecord;

public class CUBarRenderer extends XYBarRenderer {

	private static final long serialVersionUID = -6690387995050346737L;

	public CUBarRenderer() {
		init();
	}

	public CUBarRenderer(double margin) {
		super(margin);
		init();
	}

	private void init() {
		setShadowVisible(false);
		codonPositions_ = new HashMap<CodonMapKey, Integer>();
		colorForSearchPositions_ = new HashMap<Integer, Color>();
	}

	@Override
	public Paint getItemPaint(int row, int column) {
		if (column == selectedItemColumn_ && row == selectedItemRow_) {
			return Color.BLUE;
		} else if (searchPositions_ != null && searchPositions_.size() > 0) {
			Integer posInGenome = codonPositions_.get(new CodonMapKey(row, column));
			if (searchPositions_.contains(posInGenome)) {
				return colorForSearchPositions_.get(posInGenome);
			} else {
				return super.getItemPaint(row, column);
			}
		} else {
			return super.getItemPaint(row, column);
		}
	}

	public void setSelectedItemCoordinates(int series, int col) {
		selectedItemColumn_ = col;
		selectedItemRow_ = series;
	}

	public void setSearchPositions(List<Integer> searchPositions) {
		searchPositions_ = searchPositions;
	}
	
	public Map<CodonMapKey, Integer> getCodonPositions() {
		return codonPositions_;
	}
	
	public Map<Integer, Color> getColorForSearchPositions() {
		return colorForSearchPositions_;
	}

	private int selectedItemColumn_ = -1;
	private int selectedItemRow_ = -1;
	private Map<CodonMapKey, Integer> codonPositions_;
	private List<Integer> searchPositions_;
	private Map<Integer, Color> colorForSearchPositions_;
}
