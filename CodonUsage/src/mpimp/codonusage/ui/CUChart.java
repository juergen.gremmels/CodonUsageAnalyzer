/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.ui;

import java.awt.Color;
import java.awt.Shape;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.util.ShapeUtilities;

import mpimp.codonusage.db.CodonRecord;
import mpimp.codonusage.db.CodonUsageModel;
import mpimp.codonusage.util.CUXYSeriesComparator;
import mpimp.codonusage.util.ColorFinder;
import mpimp.codonusage.util.SequenceCalculator;

public class CUChart {

	public static enum ChartType {
		SCATTER_PLOT, BAR_PLOT, MOVING_AVERAGE
	};

	public CUChart(CodonUsageModel cuModel) {
		cuModel_ = cuModel;
		scatterPlotRenderer_ = new CULineAndShapeRenderer();
		barGraphRenderer_ = new CUBarRenderer();
		colorFinder_ = new ColorFinder(cuModel_);
	}

	public void initializePlot(String title) {
		clearDataSeriesContainers();
		refreshDataset();
		if (cuModel_.getChartType() == ChartType.SCATTER_PLOT) {
			currentChart_ = ChartFactory.createScatterPlot(title, "Position in analysis sequence", "rel. adaptiveness",
					dataset_, PlotOrientation.VERTICAL, true, true, false);
		} else {
			currentChart_ = ChartFactory.createXYBarChart(title, "Position in analysis sequence", false,
					"rel. adaptiveness", dataset_, PlotOrientation.VERTICAL, true, true, false);
		}

		diamond_ = ShapeUtilities.createDiamond(3);
		upTriangle_ = ShapeUtilities.createUpTriangle(5);

		chartPanel_ = new ChartPanel(currentChart_);
		chartPanel_.setPreferredSize(new java.awt.Dimension(700, 400));
		plot_ = currentChart_.getXYPlot();
		plot_.setBackgroundPaint(Color.WHITE);
		plot_.setRangeGridlinePaint(Color.GRAY);
		plot_.setDomainGridlinePaint(Color.GRAY);
		plot_.getRangeAxis().setRange(0.0, 1.05);

		paintDataSeries();

		if (cuModel_.getChartType() == ChartType.SCATTER_PLOT || cuModel_.getChartType() == ChartType.MOVING_AVERAGE) {
			plot_.setRenderer(scatterPlotRenderer_);
		} else {
			plot_.setRenderer(barGraphRenderer_);
		}
	}

	public void paintSearchResultSeries() {
		if (cuModel_.getSearchPatternList() != null) {
			int searchResultSeriesCounter = seriesIndex_;
			for (int i = 0; i < cuModel_.getSearchPatternList().size(); i++) {
				if (cuModel_.getChartType() == ChartType.SCATTER_PLOT) {
					scatterPlotRenderer_.setSeriesPaint(searchResultSeriesCounter,
							cuModel_.getColorsForSearchPatternMap().get(cuModel_.getSearchPatternList().get(i)));
					scatterPlotRenderer_.setSeriesLinesVisible(searchResultSeriesCounter, false);
					scatterPlotRenderer_.setSeriesShape(searchResultSeriesCounter, upTriangle_);
					if (cuModel_.getUseTripletFrame() == true) {
						scatterPlotRenderer_.setSeriesToolTipGenerator(searchResultSeriesCounter,
								new CUTooltipGenerator(cuModel_.getCurCollectionForAnalysis(),
										cuModel_.getPositions()));
					}
				} else {
					if (cuModel_.getUseTripletFrame() == true) {
						barGraphRenderer_.setSeriesToolTipGenerator(searchResultSeriesCounter, new CUTooltipGenerator(
								cuModel_.getCurCollectionForAnalysis(), cuModel_.getPositions()));
						barGraphRenderer_.setSeriesPaint(searchResultSeriesCounter,
								cuModel_.getColorsForSearchPatternMap().get(cuModel_.getSearchPatternList().get(i)));
					}
				}
				searchResultSeriesCounter++;
			}
			seriesIndex_ = searchResultSeriesCounter;
		}
	}

	public void paintDataSeries() {
		int dataSeriesCounter = seriesIndex_;
		for (CUXYSeries currentDataSeries : cuModel_.getDataSeriesMap().values()) {
			Color color = currentDataSeries.getDisplayColor();
			if (color == null) {
				color = new Color((int) (Math.random() * 0x1000000));
			}
			if (cuModel_.getChartType() == ChartType.SCATTER_PLOT) {
				scatterPlotRenderer_.setSeriesToolTipGenerator(dataSeriesCounter,
						new CUTooltipGenerator(cuModel_.getCurCollectionForAnalysis(), cuModel_.getPositions()));
				scatterPlotRenderer_.setSeriesPaint(dataSeriesCounter, color);
				scatterPlotRenderer_.setSeriesLinesVisible(dataSeriesCounter, false);
				scatterPlotRenderer_.setSeriesShape(dataSeriesCounter, diamond_);
			} else {
				barGraphRenderer_.setSeriesToolTipGenerator(dataSeriesCounter,
						new CUTooltipGenerator(cuModel_.getCurCollectionForAnalysis(), cuModel_.getPositions()));
				barGraphRenderer_.setSeriesPaint(dataSeriesCounter, color);
			}
			dataSeriesCounter++;
		}
		seriesIndex_ = dataSeriesCounter;
	}

	public ChartPanel getChartPanel() {
		return chartPanel_;
	}

	public void refreshSearchPattern() {
		String[] searchPatterns = cuModel_.getSearchString().split(",");
		cuModel_.setSearchPatternList(new ArrayList<String>());
		for (int i = 0; i < searchPatterns.length; i++) {
			String singlePattern = searchPatterns[i].trim();
			if (!singlePattern.equals("")) {
				cuModel_.getSearchPatternList().add(singlePattern);
			}
		}
	}

	public void markSelectedItem(int series, int col) {
		if (cuModel_.getChartType() == ChartType.SCATTER_PLOT) {
			scatterPlotRenderer_.setSeletedItemCoordinates(series, col);
		} else {
			barGraphRenderer_.setSelectedItemCoordinates(series, col);
		}
	}

	public void refreshDataset() {
		seriesIndex_ = 0;
		int seriesCounter = 0;
		if (dataset_ == null) {
			dataset_ = new XYSeriesCollection();
		}

		CUXYSeries dataSeries = null;
		//int wholeSequenceLength = cuModel_.getWholeGenomeSequence().length();
		int wholeSequenceLength = cuModel_.getWholeGenomeSequenceAsReference().length();

		CodonRecord currentCodon = null;

		for (Integer i = 1; i <= wholeSequenceLength - 2; i++) {
			currentCodon = cuModel_.getCodonCollectionForAnalysis().get(i);
			if (currentCodon != null) {
				if (cuModel_.getDataSeriesMap().containsKey(currentCodon.getAnnotationOfSourceSequence())) {
					CUXYSeries currentSeries = cuModel_.getDataSeriesMap()
							.get(currentCodon.getAnnotationOfSourceSequence());
					currentSeries.add(new CUXYDataItem(currentCodon.getPositionInAnalysisSequence(),
							currentCodon.getCodonUsageRecord().getAdaptivenessRelToAnalysisSequence(),
							currentCodon.getPositionInGenome()));
				} else {
					dataSeries = new CUXYSeries(currentCodon.getAnnotationOfSourceSequence(), seriesCounter);
					seriesCounter++;
					dataSeries.setDisplayColor(currentCodon.getDisplayColor());
					dataSeries.add(new CUXYDataItem(currentCodon.getPositionInAnalysisSequence(),
							currentCodon.getCodonUsageRecord().getAdaptivenessRelToAnalysisSequence(),
							currentCodon.getPositionInGenome()));
					cuModel_.getDataSeriesMap().put(currentCodon.getAnnotationOfSourceSequence(), dataSeries);
				}
			}
		}

		Collection<CUXYSeries> seriesCollection = cuModel_.getDataSeriesMap().values();
		List<CUXYSeries> seriesList = new ArrayList<CUXYSeries>();
		seriesList.addAll(seriesCollection);
		Collections.sort(seriesList, new CUXYSeriesComparator());

		for (CUXYSeries currentDataSeries : seriesList) {
			dataset_.addSeries(currentDataSeries);
			if (cuModel_.getChartType() == ChartType.BAR_PLOT) {
				barGraphRenderer_.getCodonPositions().putAll(currentDataSeries.getCodonPositions());
			}
		}
	}

	public void addSearchPositionsDataSeries() {
		Map<String, List<Integer>> searchPatternPositionsMap = cuModel_.getSearchPatternPositionsMap();

		List<Integer> searchPositionsInGenome = new ArrayList<Integer>();

		CUXYSeries searchPositions = null;

		if (cuModel_.getSearchPatternList() != null && cuModel_.getSearchPatternList().size() > 0
				&& searchPatternPositionsMap.size() > 0) {
			Set<String> searchPatternList = searchPatternPositionsMap.keySet();

			for (String currentPattern : searchPatternList) {
				if (!cuModel_.getColorsForSearchPatternMap().containsKey(currentPattern)) {
					Color color = colorFinder_.getColor();
					cuModel_.getColorsForSearchPatternMap().put(currentPattern, color);
				}

				String searchPatternForLegendDisplay = "";
				if (cuModel_.getUseTripletFrame() == true) {
					searchPatternForLegendDisplay = SequenceCalculator.trimStringToTripletString(currentPattern);
				} else {
					searchPatternForLegendDisplay = currentPattern.toUpperCase();
				}
				searchPositions = new CUXYSeries(searchPatternForLegendDisplay);

				for (Integer currentPosition : searchPatternPositionsMap.get(currentPattern)) {
					if (cuModel_.getChartType() == ChartType.SCATTER_PLOT) {
						searchPositions.add(currentPosition, new Double(0.0));
					}
					if (cuModel_.getChartType() == ChartType.BAR_PLOT && cuModel_.getUseTripletFrame() == true) {
						Integer positionInGenome = cuModel_.getCodonCollectionForAnalysisPosInAnalysisSequence()
								.get(currentPosition).getPositionInGenome();
						searchPositionsInGenome.add(positionInGenome);
						barGraphRenderer_.getColorForSearchPositions().put(positionInGenome,
								cuModel_.getColorsForSearchPatternMap().get(currentPattern));
					}
				}
				dataset_.addSeries(searchPositions);
				cuModel_.getSearchResultSeries().add(searchPositions);
			}
			if (cuModel_.getChartType() == ChartType.BAR_PLOT) {
				barGraphRenderer_.setSearchPositions(searchPositionsInGenome);
			}
		}
	}

	public void removeOldDataSeries() {
		for (CUXYSeries oldSeries : cuModel_.getDataSeriesMap().values()) {
			dataset_.removeSeries(oldSeries);
		}
		for (CUXYSeries oldSeries : cuModel_.getSearchResultSeries()) {
			dataset_.removeSeries(oldSeries);
		}
		clearDataSeriesContainers();
	}

	public void clearDataSeriesContainers() {
		cuModel_.getDataSeriesMap().clear();
		cuModel_.getSearchResultSeries().clear();
	}
	
	private CodonUsageModel cuModel_;

	private ChartPanel chartPanel_;
	private JFreeChart currentChart_;
	private XYPlot plot_;
	private CULineAndShapeRenderer scatterPlotRenderer_;
	private CUBarRenderer barGraphRenderer_;
	private Shape diamond_;
	private Shape upTriangle_;
	private Integer seriesIndex_ = 0;
	private XYSeriesCollection dataset_ = null;
	private ColorFinder colorFinder_;
}
