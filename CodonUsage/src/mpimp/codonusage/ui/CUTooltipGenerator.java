/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.ui;

import java.text.DecimalFormat;
import java.util.Map;

import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.data.xy.XYDataset;

import mpimp.codonusage.db.CodonUsageRecord;

public class CUTooltipGenerator extends StandardXYToolTipGenerator {

	private static final long serialVersionUID = -8455044098696465970L;

	public CUTooltipGenerator() {
		// TODO Auto-generated constructor stub
	}

	public CUTooltipGenerator(Map<String, CodonUsageRecord> codonCollection, Map<Integer, String> positions) {
		codonCollection_ = codonCollection;
		positions_ = positions;
	}

	@Override
	public String generateToolTip(XYDataset dataset, int series, int item) {
		Number positionNumber = dataset.getX(series, item); // convert from 0..(n-1) to 1..n
		Integer position = -1;
		if (positionNumber instanceof Integer) {
			position = (Integer)positionNumber;
		}
		CodonUsageRecord cur = codonCollection_.get(positions_.get(position));
		DecimalFormat df = new DecimalFormat("#.##");
		String tooltip = "";
		if (cur != null) {
			tooltip += "Codon: " + cur.getCodon();
			tooltip += "; ";
			tooltip += "Frequency: " + df.format(cur.getFrequency());
			tooltip += "; ";
			tooltip += "Rel. Adaptiveness: " + df.format(cur.getAdaptivenessRelToAnalysisSequence());
		}
		return tooltip;
	}

	private Map<String, CodonUsageRecord> codonCollection_;
	Map<Integer, String> positions_;
}
