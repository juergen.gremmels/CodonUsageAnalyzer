/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.ui;

import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.data.xy.XYSeries;

import mpimp.codonusage.db.CodonMapKey;

public class CUXYSeries extends XYSeries {

	public CUXYSeries(Comparable key) {
		super(key);
	}

	public CUXYSeries(Comparable key, boolean autoSort) {
		super(key, autoSort);
	}

	public CUXYSeries(Comparable key, boolean autoSort, boolean allowDuplicateXValues) {
		super(key, autoSort, allowDuplicateXValues);
	}
	
	public CUXYSeries(Comparable key, Integer seriesIndex) {
		super(key);
		seriesIndex_ = seriesIndex;
	}
	
	public Color getDisplayColor() {
		return displayColor_;
	}

	public void setDisplayColor(Color displayColor) {
		displayColor_ = displayColor;
	}
	
	public Integer getSeriesIndex() {
		return seriesIndex_;
	}
	
	public Map<CodonMapKey, Integer> getCodonPositions() {
		codonPositions_ = new HashMap<CodonMapKey, Integer>();
		List<CUXYDataItem> items = (List<CUXYDataItem>)getItems();
		for (CUXYDataItem currentItem : items) {
			if (currentItem instanceof CUXYDataItem) {
				CUXYDataItem cuxyItem = (CUXYDataItem) currentItem;
				CodonMapKey cmKey = new CodonMapKey(seriesIndex_, indexOf(cuxyItem.getX()));
				codonPositions_.put(cmKey, currentItem.getPositionInGenome());
			}
		}
		return codonPositions_;
	}

	private Color displayColor_ = null;
	private Integer seriesIndex_ = -1;
	private Map<CodonMapKey, Integer> codonPositions_;

}
