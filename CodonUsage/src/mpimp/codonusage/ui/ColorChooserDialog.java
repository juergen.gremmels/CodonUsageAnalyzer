/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import mpimp.codonusage.db.AnnotationRecord;
import mpimp.codonusage.db.CodonUsageModel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

public class ColorChooserDialog extends JDialog {

	public void showDialog() {
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setModal(true);
		this.setVisible(true);
		return;
	}

	/**
	 * Create the dialog.
	 */
	public ColorChooserDialog(CodonUsageModel cuModel) {
		setFont(new Font("Dialog", Font.BOLD, 12));
		setTitle("Colors for data series");
		cuModel_ = cuModel;
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel_.setLayout(new BoxLayout(contentPanel_, BoxLayout.Y_AXIS));
		contentPanel_.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel_, BorderLayout.CENTER);
		{
			JLabel lblYouMayAccept = new JLabel("You may accept the default selection or select your own colors for the data series");
			lblYouMayAccept.setHorizontalAlignment(SwingConstants.LEFT);
			lblYouMayAccept.setHorizontalTextPosition(SwingConstants.LEFT);
			lblYouMayAccept.setFont(new Font("Tahoma", Font.PLAIN, 11));
			contentPanel_.add(lblYouMayAccept);
		}

		colorChooserCBs_ = new ArrayList<TextWithComboBoxPanel>();

		{
			notification_ = new Notification();

			List<AnnotationRecord> sequencesForAnalysis = cuModel_.getSequencesForAnalysis();
			int index = 0;
			for (AnnotationRecord currentRecord : sequencesForAnalysis) {
				TextWithComboBoxPanel twcbPanel = new TextWithComboBoxPanel(currentRecord);
				twcbPanel.setSelectedItem(index);
				index++;
				notification_.addObserver(twcbPanel);
				contentPanel_.add(twcbPanel);
				colorChooserCBs_.add(twcbPanel);//for removing later when updating the plot
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				final JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						if (e.getSource().equals(okButton)) {
							notification_.collectData();
							for (TextWithComboBoxPanel twc : colorChooserCBs_) {
								contentPanel_.remove(twc);
							}
							colorChooserCBs_.clear();
							setVisible(false);
							dispose();
						}
					}

				});
			}
			{
				final JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						if (e.getSource().equals(cancelButton)) {
							for (TextWithComboBoxPanel twc : colorChooserCBs_) {
								contentPanel_.remove(twc);
							}
							colorChooserCBs_.clear();
							setVisible(false);
							dispose();
						}
					}
				});
			}
		}
	}

	private final JPanel contentPanel_ = new JPanel();
	private Notification notification_;
	private CodonUsageModel cuModel_;
	private List<TextWithComboBoxPanel> colorChooserCBs_;
}
