/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.ui;

import java.awt.Color;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class DataInfoPanel extends JPanel {

	/**
	 * Create the panel.
	 */
	public DataInfoPanel() {

		codonNameDiscLabel_ = new JLabel("Codon:");

		codonNameLabel_ = new JLabel("");
		codonNameLabel_.setOpaque(true);
		codonNameLabel_.setBackground(Color.WHITE);

		posInGenomeDiscLabel_ = new JLabel("Pos. in genome:");

		posInGenomeLabel_ = new JLabel("");
		posInGenomeLabel_.setOpaque(true);
		posInGenomeLabel_.setBackground(Color.WHITE);

		posInSequenceDiscLabel_ = new JLabel("Pos. in sequence");

		posInSequenceLabel_ = new JLabel("");
		posInSequenceLabel_.setOpaque(true);
		posInSequenceLabel_.setBackground(Color.WHITE);

		relAdaptivenessDiscLabel_ = new JLabel("rel. Adaptiveness:");

		relAdaptivenessLabel_ = new JLabel("");
		relAdaptivenessLabel_.setOpaque(true);
		relAdaptivenessLabel_.setBackground(Color.WHITE);

		revComplementDiscLabel_ = new JLabel("from reverse complement:");

		revComplementLabel_ = new JLabel("");
		revComplementLabel_.setOpaque(true);
		revComplementLabel_.setBackground(Color.WHITE);

		annotationDiscLabel_ = new JLabel("annotation:");

		annotationLabel_ = new JLabel("");
		annotationLabel_.setOpaque(true);
		annotationLabel_.setBackground(Color.WHITE);

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addComponent(codonNameDiscLabel_).addGap(18)
						.addComponent(codonNameLabel_).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(posInGenomeDiscLabel_).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(posInGenomeLabel_).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(posInSequenceDiscLabel_).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(posInSequenceLabel_).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(relAdaptivenessDiscLabel_).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(relAdaptivenessLabel_).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(revComplementDiscLabel_).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(revComplementLabel_).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(annotationDiscLabel_).addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(annotationLabel_)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addGap(12)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(codonNameDiscLabel_)
								.addComponent(codonNameLabel_).addComponent(posInGenomeDiscLabel_)
								.addComponent(posInGenomeLabel_).addComponent(posInSequenceDiscLabel_)
								.addComponent(posInSequenceLabel_).addComponent(relAdaptivenessDiscLabel_)
								.addComponent(relAdaptivenessLabel_).addComponent(revComplementDiscLabel_)
								.addComponent(revComplementLabel_).addComponent(annotationDiscLabel_)
								.addComponent(annotationLabel_))));
		setLayout(groupLayout);

	}

	public JLabel getCodonNameDiscLabel() {
		return codonNameDiscLabel_;
	}

	public JLabel getCodonNameLabel() {
		return codonNameLabel_;
	}

	public JLabel getPosInGenomeDiscLabel() {
		return posInGenomeDiscLabel_;
	}

	public JLabel getPosInGenomeLabel() {
		return posInGenomeLabel_;
	}

	public JLabel getPosInSequenceDiscLabel() {
		return posInSequenceDiscLabel_;
	}

	public JLabel getPosInSequenceLabel() {
		return posInSequenceLabel_;
	}

	public JLabel getRelAdaptivenessDiscLabel() {
		return relAdaptivenessDiscLabel_;
	}

	public JLabel getRelAdaptivenessLabel() {
		return relAdaptivenessLabel_;
	}

	public JLabel getRevComplementLabel() {
		return revComplementLabel_;
	}

	public JLabel getAnnotationLabel() {
		return annotationLabel_;
	}

	private JLabel codonNameDiscLabel_;
	private JLabel codonNameLabel_;
	private JLabel posInGenomeDiscLabel_;
	private JLabel posInGenomeLabel_;
	private JLabel posInSequenceDiscLabel_;
	private JLabel posInSequenceLabel_;
	private JLabel relAdaptivenessDiscLabel_;
	private JLabel relAdaptivenessLabel_;
	private JLabel revComplementDiscLabel_;
	private JLabel revComplementLabel_;
	private JLabel annotationDiscLabel_;
	private JLabel annotationLabel_;
}
