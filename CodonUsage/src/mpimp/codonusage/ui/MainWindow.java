/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.data.xy.XYDataset;

import mpimp.codonusage.db.AnnotationRecord;
import mpimp.codonusage.db.CodonRecord;
import mpimp.codonusage.db.CodonUsageModel;
import mpimp.codonusage.ui.CUChart.ChartType;
import mpimp.codonusage.util.CUAnalyzer;
import mpimp.codonusage.util.FastaAnnotationParser;
import mpimp.codonusage.util.GenbankAnnotationParser;

public class MainWindow {

	public static enum InputFileType {
		GENBANK, FASTA
	}

	private final JFrame frame_ = new JFrame();

	private JComponent dataInputPanel_;
	private JPanel chartPanel_;
	private CUChart cuChart_;
	private DataInfoPanel dataInfoPanel_;
	private SearchPanel searchPanel_;
	private JPanel plotPanel_;
	private JTabbedPane tabbedPane_;
	private JMenuBar menuBar_;
	private JMenu mnFileAnalysis_;
	private JMenu mnFileReference_;
	private JMenuItem mntmOpenAnalysisGenbank_;
	private JMenuItem mntmOpenAnalysisFasta_;
	private JMenuItem mntmPasteTextAnalysis_;
	private JMenuItem mntmOpenReferenceGenbank_;
	private JMenuItem mntmOpenReferenceFasta_;
	private JMenuItem mntmPasteTextReference_;

	private JCheckBoxList<JCheckboxWithObject> geneSelectionListAnalysis_;
	private JCheckBoxList<JCheckboxWithObject> geneSelectionListReference_;
	private JScrollPane geneSelectionScrollPaneAnalysis_;
	private JScrollPane geneSelectionScrollPaneReference_;
	private JScrollPane searchAndDataPanelScrollPane_;
	private JPanel searchAndDataPanelPanel_;
	private JButton showOrUpdatePlotButton_;
	private JButton printTableButton_;
	private JButton clearAllDataButton_;
	private JRadioButton scatterPlotRadioButton_;
	private JRadioButton barPlotRadioButton_;
	private JRadioButton slidingMeanRadioButton_;
	private ButtonGroup plotTypeButtons_;

	private CodonUsageModel cuModel_;
	private CUAnalyzer cuAnalyzer_;
	private JMenu mnExport_;
	private JMenu mnPrintTable_;
	private JMenuItem mntmAllData_;
	private JMenuItem mntmSelectedCodons_;
	private JPanel dataControlPanelAnalysis_;
	private JPanel dataControlPanelReference_;
	private JButton selectAllAnalysisButton_;
	private JButton deselectAllAnalysisButton_;
	private JButton selectAllReferenceButton_;
	private JButton deselectAllReferenceButton_;
	private JPanel dataControlPanel_;
	private JButton exitButton_;

	/**
	 * Create the application.
	 */
	public MainWindow() {
		cuModel_ = new CodonUsageModel();
		cuAnalyzer_ = new CUAnalyzer(cuModel_);
		initGUI();
		initActionListeners();
	}

	public void show() {
		frame_.setVisible(true);
	}

	public JFrame getFrame() {
		return frame_;
	}

	public void refreshChart(Boolean resetBoundaries) {
		if (resetBoundaries == true) {
			cuModel_.setLeftBoundary(-1);
			cuModel_.setRightBoundary(-1);
		}
		cuAnalyzer_.refreshBasicCalculation();
		cuChart_ = new CUChart(cuModel_);
		cuChart_.initializePlot("Codon Usage");
		plotPanel_ = new JPanel();
		plotPanel_.setLayout(new BoxLayout(plotPanel_, BoxLayout.Y_AXIS));

		chartPanel_ = cuChart_.getChartPanel();

		((ChartPanel) chartPanel_).addChartMouseListener(new ChartMouseListener() {

			@Override
			public void chartMouseMoved(ChartMouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void chartMouseClicked(ChartMouseEvent e) {
				handleChartMouseEvent(e);
			}
		});

		dataInfoPanel_ = new DataInfoPanel();
		searchPanel_ = new SearchPanel(this, cuModel_);
		plotPanel_.add(chartPanel_);

		searchAndDataPanelPanel_ = new JPanel();
		searchAndDataPanelPanel_.setLayout(new BoxLayout(searchAndDataPanelPanel_, BoxLayout.Y_AXIS));
		searchAndDataPanelPanel_.add(dataInfoPanel_);
		searchAndDataPanelPanel_.add(searchPanel_);
		searchAndDataPanelScrollPane_ = new JScrollPane(searchAndDataPanelPanel_);
		plotPanel_.add(searchAndDataPanelScrollPane_);

		if (cuModel_.getChartType() == ChartType.BAR_PLOT) {
			searchPanel_.getUseTripletFrameCheckBox().setSelected(true);
			searchPanel_.getUseTripletFrameCheckBox().setVisible(false);
		} else {
			searchPanel_.getUseTripletFrameCheckBox().setVisible(true);
		}

		if (tabbedPane_.getTabCount() > 1 && tabbedPane_.getComponentAt(1) != null) {
			tabbedPane_.removeTabAt(1);
		}
		tabbedPane_.addTab("Plot", plotPanel_);
		frame_.revalidate();
		frame_.repaint();
	}

	public void refreshSearchResults() {
		cuChart_.removeOldDataSeries();
		cuAnalyzer_.refreshBasicCalculation();
		cuChart_.refreshDataset();
		cuChart_.refreshSearchPattern();
		cuModel_.setSearchPatternPositionsMap(cuAnalyzer_.getSearchPatternPositionsMap());
		cuChart_.paintDataSeries();
		cuChart_.addSearchPositionsDataSeries();
		cuChart_.paintSearchResultSeries();
		chartPanel_.repaint();
	}

	private void handleChartMouseEvent(ChartMouseEvent e) {
		if (e != null) {
			if (e.getEntity() instanceof XYItemEntity) {
				XYItemEntity xyit = (XYItemEntity) e.getEntity();
				int item = xyit.getItem();
				XYDataset xyd = xyit.getDataset();
				Number xValue = xyd.getX(xyit.getSeriesIndex(), item);

				NumberFormat nf = NumberFormat.getInstance();
				nf.setMaximumFractionDigits(2);

				for (CodonRecord currentRecord : cuModel_.getCodonCollectionForAnalysis().values()) {
					if (currentRecord.getPositionInAnalysisSequence().equals(xValue)) {
						dataInfoPanel_.getCodonNameLabel().setText(currentRecord.getCodon());
						dataInfoPanel_.getPosInGenomeLabel().setText(currentRecord.getPositionInGenome().toString());
						dataInfoPanel_.getPosInSequenceLabel()
								.setText(currentRecord.getPositionInAnalysisSequence().toString());
						dataInfoPanel_.getRelAdaptivenessLabel().setText(
								nf.format(currentRecord.getCodonUsageRecord().getAdaptivenessRelToAnalysisSequence()));
						dataInfoPanel_.getRevComplementLabel()
								.setText(currentRecord.getFromReverseComplement().toString());
						dataInfoPanel_.getAnnotationLabel().setText(currentRecord.getAnnotationOfSourceSequence());
						cuModel_.setSelectedCodonRecord(currentRecord);
						cuChart_.markSelectedItem(xyit.getSeriesIndex(), item);
						chartPanel_.repaint();
					}
				}
			}
		}
	}

	private void loadAnalysisData(InputFileType inputFileType) {
		if (chooseSourceFile(inputFileType) == true) {
			try {
				loadAnnotationsFromFile(inputFileType);
				fillGeneSelectionListForAnalysis();
				cuAnalyzer_.readEntireSequenceFromInputFile(inputFileType);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	private void loadReferenceData(InputFileType inputFileType) {
		if (chooseSourceFile(inputFileType) == true) {
			try {
				loadAnnotationsFromFileAsReference(inputFileType);
				fillGeneSelectionListAsReference();
				cuAnalyzer_.readEntireSequenceFromInputFileAsReference(inputFileType);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private Boolean chooseSourceFile(InputFileType fileType) {
		JFileChooser fileChooser = null;
		if (cuModel_.getLastOpenedDirPath().equals("")) {
			fileChooser = new JFileChooser();
		} else {
			fileChooser = new JFileChooser(cuModel_.getLastOpenedDirPath());
		}
		FileNameExtensionFilter inputFormatFilter = null;
		if (fileType == InputFileType.GENBANK) {
			inputFormatFilter = new FileNameExtensionFilter("Genbank files", "gb", "gbk");
		} else if (fileType == InputFileType.FASTA) {
			inputFormatFilter = new FileNameExtensionFilter("FASTA files", "fa", "fasta", "fas", "fna");
		}
		fileChooser.setFileFilter(inputFormatFilter);
		int retVal = fileChooser.showOpenDialog(frame_);
		if (retVal == JFileChooser.APPROVE_OPTION) {
			cuModel_.setSourceFilePath(fileChooser.getSelectedFile().getAbsolutePath());
			cuModel_.setLastOpenedDirPath(
					extractDirectoryPathFromFilePath(fileChooser.getSelectedFile().getAbsolutePath()));
			return true;

		}
		return false;
	}

	private void loadAnnotationsFromFile(InputFileType inputFileType) throws Exception {
		if (inputFileType == InputFileType.GENBANK) {
			cuModel_.getLoadedSequencesForAnalysis()
					.addAll(GenbankAnnotationParser.parseAnnotations(cuModel_.getSourceFilePath()));
		} else if (inputFileType == InputFileType.FASTA) {
			List<AnnotationRecord> annotationRecords = FastaAnnotationParser.parseAnnotations(cuModel_.getSourceFilePath());
			for (AnnotationRecord annotationRecord : annotationRecords) {
				constructFreeTextCodonPositionsForAnalysis(annotationRecord);
			}
		}
	}

	private void loadAnnotationsFromFileAsReference(InputFileType inputFileType) throws Exception {
		if (inputFileType == InputFileType.GENBANK) {
			cuModel_.getLoadedSequencesAsReference()
					.addAll(GenbankAnnotationParser.parseAnnotations(cuModel_.getSourceFilePath()));
		} else if (inputFileType == InputFileType.FASTA) {
			List<AnnotationRecord> annotationRecords = FastaAnnotationParser.parseAnnotations(cuModel_.getSourceFilePath());
			for (AnnotationRecord annotationRecord : annotationRecords) {
				constructFreeTextCodonPositionsAsReference(annotationRecord);
			}
		}
	}

	private void fillGeneSelectionListForAnalysis() {
		DefaultListModel<JCheckboxWithObject> geneSelectionListModel = new DefaultListModel<JCheckboxWithObject>();
		geneSelectionListAnalysis_.setModel(geneSelectionListModel);
		for (AnnotationRecord currentRecord : cuModel_.getLoadedSequencesForAnalysis()) {
			JCheckboxWithObject geneCheckbox = new JCheckboxWithObject(currentRecord);
			geneSelectionListModel.addElement(geneCheckbox);
		}
	}

	private void fillGeneSelectionListAsReference() {
		DefaultListModel<JCheckboxWithObject> geneSelectionListModel = new DefaultListModel<JCheckboxWithObject>();
		geneSelectionListReference_.setModel(geneSelectionListModel);
		for (AnnotationRecord currentRecord : cuModel_.getLoadedSequencesAsReference()) {
			JCheckboxWithObject geneCheckbox = new JCheckboxWithObject(currentRecord);
			geneSelectionListModel.addElement(geneCheckbox);
		}
	}

	private void exportDataForSelectedCodons() {
		SelectCodonsForExportDialog scfeDialog = new SelectCodonsForExportDialog(frame_, cuModel_);
		if (scfeDialog.showDialog() == true) {
			chooseFileForResultTable();
		}
	}

	private AnnotationRecord showSequenceInputDialog() {
		SequenceInputDialog dialog = new SequenceInputDialog(frame_);
		return dialog.showDialog();
	}

	private void fetchSequencePastedAsTextForAnalysis() {
		AnnotationRecord annotationRecord = showSequenceInputDialog();
		if (annotationRecord != null) {
			constructFreeTextCodonPositionsForAnalysis(annotationRecord);
			String wholeGenomeSequence = cuModel_.getWholeGenomeSequenceForAnalysis();
			wholeGenomeSequence += annotationRecord.getPartSequence();
			cuModel_.setWholeGenomeSequenceForAnalysis(wholeGenomeSequence);
			fillGeneSelectionListForAnalysis();
		}
	}
	
	private void constructFreeTextCodonPositionsForAnalysis(AnnotationRecord annotationRecord) {
		Integer position = cuModel_.getPositionCounterForFreeTextSequence();
		annotationRecord.getJoinPartItems().get(0).setStartPosition(position);
		position += annotationRecord.getPartSequence().length();
		annotationRecord.getJoinPartItems().get(0).setEndPosition(position);
		cuModel_.setPositionCounterForFreeTextSequence(position);
		cuModel_.getLoadedSequencesForAnalysis().add(annotationRecord);
	}

	private void fetchSequencePastedAsTextAsReference() {
		AnnotationRecord annotationRecord = showSequenceInputDialog();
		if (annotationRecord != null) {
			constructFreeTextCodonPositionsAsReference(annotationRecord);
			String wholeGenomeSequence = cuModel_.getWholeGenomeSequenceAsReference();
			wholeGenomeSequence += annotationRecord.getPartSequence();
			cuModel_.setWholeGenomeSequenceAsReference(wholeGenomeSequence);
			fillGeneSelectionListAsReference();
		}
	}
	
	private void constructFreeTextCodonPositionsAsReference(AnnotationRecord annotationRecord) {
		Integer position = cuModel_.getPositionCounterForFreeTextSequenceAsReference();
		annotationRecord.getJoinPartItems().get(0).setStartPosition(position);
		position += annotationRecord.getPartSequence().length();
		annotationRecord.getJoinPartItems().get(0).setEndPosition(position);
		cuModel_.setPositionCounterForFreeTextSequenceAsReference(position);
		cuModel_.getLoadedSequencesAsReference().add(annotationRecord);
	}

	private void chooseFileForResultTable() {
		JFileChooser fileChooser = null;
		if (cuModel_.getLastOpenedDirPath().equals("")) {
			fileChooser = new JFileChooser();
		} else {
			fileChooser = new JFileChooser(cuModel_.getLastOpenedDirPath());
		}

		int retVal = fileChooser.showSaveDialog(frame_);
		if (retVal == JFileChooser.APPROVE_OPTION) {
			File info = new File(fileChooser.getSelectedFile().getAbsolutePath());
			if (info.exists()) {
				if (JOptionPane.showConfirmDialog(frame_, "Overwrite?", "File exists!",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					try {
						info.delete();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				} else {
					return;
				}
			}
			cuModel_.setTableFilePath(fileChooser.getSelectedFile().getAbsolutePath());
			cuModel_.setLastOpenedDirPath(
					extractDirectoryPathFromFilePath(fileChooser.getSelectedFile().getAbsolutePath()));
			try {
				cuAnalyzer_.printTable();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	private String extractDirectoryPathFromFilePath(String filePath) {
		String directoryPath = "";
		File lastOpened = new File(filePath);
		directoryPath = lastOpened.getParent();
		return directoryPath;
	}

	private void initGUI() {
		frame_.setBounds(100, 100, 902, 596);
		frame_.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame_.getContentPane().setLayout(new BorderLayout(0, 0));

		tabbedPane_ = new JTabbedPane(JTabbedPane.TOP);
		frame_.getContentPane().add(tabbedPane_, BorderLayout.CENTER);

		dataInputPanel_ = new JPanel(false);
		GridBagLayout gbl_dataInputPanel = new GridBagLayout();
		gbl_dataInputPanel.columnWidths = new int[] { 320, 100, 320, 0 };
		gbl_dataInputPanel.rowHeights = new int[] { 0, 293, 0 };
		gbl_dataInputPanel.columnWeights = new double[] { 1.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_dataInputPanel.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		dataInputPanel_.setLayout(gbl_dataInputPanel);

		dataControlPanelAnalysis_ = new JPanel();
		// dataControlPanelAnalysis_.setMaximumSize(new Dimension(100, 32767));
		GridBagConstraints gbc_dataControlPanelAnalysis = new GridBagConstraints();
		gbc_dataControlPanelAnalysis.anchor = GridBagConstraints.NORTH;
		gbc_dataControlPanelAnalysis.insets = new Insets(0, 0, 5, 5);
		gbc_dataControlPanelAnalysis.fill = GridBagConstraints.HORIZONTAL;
		gbc_dataControlPanelAnalysis.gridx = 0;
		gbc_dataControlPanelAnalysis.gridy = 0;
		dataInputPanel_.add(dataControlPanelAnalysis_, gbc_dataControlPanelAnalysis);
		GridBagLayout gbl_dataControlPanelAnalysis = new GridBagLayout();
		gbl_dataControlPanelAnalysis.columnWidths = new int[] { 75, 12, 125, 0 };
		gbl_dataControlPanelAnalysis.rowHeights = new int[] { 20, 23, 0 };
		gbl_dataControlPanelAnalysis.columnWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_dataControlPanelAnalysis.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		dataControlPanelAnalysis_.setLayout(gbl_dataControlPanelAnalysis);

		JLabel lblDataForAnalyis = new JLabel("Data for analyis");
		lblDataForAnalyis.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblDataForAnalyis = new GridBagConstraints();
		gbc_lblDataForAnalyis.anchor = GridBagConstraints.NORTH;
		gbc_lblDataForAnalyis.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblDataForAnalyis.insets = new Insets(0, 0, 5, 0);
		gbc_lblDataForAnalyis.gridwidth = 3;
		gbc_lblDataForAnalyis.gridx = 0;
		gbc_lblDataForAnalyis.gridy = 0;
		dataControlPanelAnalysis_.add(lblDataForAnalyis, gbc_lblDataForAnalyis);

		selectAllAnalysisButton_ = new JButton("Select all");
		GridBagConstraints gbc_selectAllAnalysisButton = new GridBagConstraints();
		gbc_selectAllAnalysisButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_selectAllAnalysisButton.insets = new Insets(0, 0, 0, 5);
		gbc_selectAllAnalysisButton.gridx = 0;
		gbc_selectAllAnalysisButton.gridy = 1;
		dataControlPanelAnalysis_.add(selectAllAnalysisButton_, gbc_selectAllAnalysisButton);

		deselectAllAnalysisButton_ = new JButton("Deselect all");
		GridBagConstraints gbc_deselectAllAnalysisButton = new GridBagConstraints();
		gbc_deselectAllAnalysisButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_deselectAllAnalysisButton.gridx = 2;
		gbc_deselectAllAnalysisButton.gridy = 1;
		dataControlPanelAnalysis_.add(deselectAllAnalysisButton_, gbc_deselectAllAnalysisButton);

		dataControlPanelReference_ = new JPanel();
		GridBagConstraints gbc_dataControlPanelReference = new GridBagConstraints();
		gbc_dataControlPanelReference.anchor = GridBagConstraints.NORTH;
		gbc_dataControlPanelReference.insets = new Insets(0, 0, 5, 5);
		gbc_dataControlPanelReference.fill = GridBagConstraints.HORIZONTAL;
		gbc_dataControlPanelReference.gridx = 2;
		gbc_dataControlPanelReference.gridy = 0;
		dataInputPanel_.add(dataControlPanelReference_, gbc_dataControlPanelReference);
		GridBagLayout gbl_dataControlPanelReference = new GridBagLayout();
		gbl_dataControlPanelReference.columnWidths = new int[] { 75, 18, 125, 0 };
		gbl_dataControlPanelReference.rowHeights = new int[] { 20, 23, 0 };
		gbl_dataControlPanelReference.columnWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_dataControlPanelReference.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		dataControlPanelReference_.setLayout(gbl_dataControlPanelReference);

		JLabel lblDataForReference = new JLabel("Data for reference");
		lblDataForReference.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblDataForReference = new GridBagConstraints();
		gbc_lblDataForReference.anchor = GridBagConstraints.NORTH;
		gbc_lblDataForReference.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblDataForReference.insets = new Insets(0, 0, 5, 0);
		gbc_lblDataForReference.gridwidth = 3;
		gbc_lblDataForReference.gridx = 0;
		gbc_lblDataForReference.gridy = 0;
		dataControlPanelReference_.add(lblDataForReference, gbc_lblDataForReference);

		selectAllReferenceButton_ = new JButton("Select all");
		GridBagConstraints gbc_selectAllReferenceButton = new GridBagConstraints();
		gbc_selectAllReferenceButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_selectAllReferenceButton.insets = new Insets(0, 0, 0, 5);
		gbc_selectAllReferenceButton.gridx = 0;
		gbc_selectAllReferenceButton.gridy = 1;
		dataControlPanelReference_.add(selectAllReferenceButton_, gbc_selectAllReferenceButton);

		deselectAllReferenceButton_ = new JButton("Deselect all");
		GridBagConstraints gbc_deselectAllReferenceButton = new GridBagConstraints();
		gbc_deselectAllReferenceButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_deselectAllReferenceButton.gridx = 2;
		gbc_deselectAllReferenceButton.gridy = 1;
		dataControlPanelReference_.add(deselectAllReferenceButton_, gbc_deselectAllReferenceButton);

		geneSelectionListAnalysis_ = new JCheckBoxList<JCheckboxWithObject>();
		geneSelectionScrollPaneAnalysis_ = new JScrollPane(geneSelectionListAnalysis_);
		GridBagConstraints gbc_geneSelectionScrollPaneAnalysis = new GridBagConstraints();
		gbc_geneSelectionScrollPaneAnalysis.fill = GridBagConstraints.BOTH;
		gbc_geneSelectionScrollPaneAnalysis.insets = new Insets(0, 0, 0, 5);
		gbc_geneSelectionScrollPaneAnalysis.gridx = 0;
		gbc_geneSelectionScrollPaneAnalysis.gridy = 1;
		dataInputPanel_.add(geneSelectionScrollPaneAnalysis_, gbc_geneSelectionScrollPaneAnalysis);

		dataControlPanel_ = new JPanel();
		dataControlPanel_.setBorder(new LineBorder(new Color(128, 128, 128), 2));
		GridBagConstraints gbc_dataControlPanel_ = new GridBagConstraints();
		gbc_dataControlPanel_.gridheight = 2;
		gbc_dataControlPanel_.fill = GridBagConstraints.VERTICAL;
		gbc_dataControlPanel_.insets = new Insets(0, 0, 0, 5);
		gbc_dataControlPanel_.gridx = 1;
		gbc_dataControlPanel_.gridy = 0;
		dataInputPanel_.add(dataControlPanel_, gbc_dataControlPanel_);

		plotTypeButtons_ = new ButtonGroup();
		GridBagLayout gbl_dataControlPanel = new GridBagLayout();
		gbl_dataControlPanel.columnWidths = new int[] { 117, 0 };
		gbl_dataControlPanel.rowHeights = new int[] { 0, 23, 23, 0, 0, 23, 0, 23, 0, 23, 0 };
		gbl_dataControlPanel.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_dataControlPanel.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
				Double.MIN_VALUE };
		dataControlPanel_.setLayout(gbl_dataControlPanel);

		barPlotRadioButton_ = new JRadioButton("Bar plot");
		plotTypeButtons_.add(barPlotRadioButton_);
		GridBagConstraints gbc_barPlotRadioButton = new GridBagConstraints();
		gbc_barPlotRadioButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_barPlotRadioButton.insets = new Insets(0, 0, 5, 0);
		gbc_barPlotRadioButton.gridx = 0;
		gbc_barPlotRadioButton.gridy = 1;
		dataControlPanel_.add(barPlotRadioButton_, gbc_barPlotRadioButton);

		scatterPlotRadioButton_ = new JRadioButton("Scatter plot");

		plotTypeButtons_.add(scatterPlotRadioButton_);

		scatterPlotRadioButton_.setSelected(cuModel_.getChartType() == ChartType.SCATTER_PLOT);
		GridBagConstraints gbc_scatterPlotRadioButton = new GridBagConstraints();
		gbc_scatterPlotRadioButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_scatterPlotRadioButton.insets = new Insets(0, 0, 5, 0);
		gbc_scatterPlotRadioButton.gridx = 0;
		gbc_scatterPlotRadioButton.gridy = 2;
		dataControlPanel_.add(scatterPlotRadioButton_, gbc_scatterPlotRadioButton);

		slidingMeanRadioButton_ = new JRadioButton("Sliding mean");

		plotTypeButtons_.add(slidingMeanRadioButton_);

		GridBagConstraints gbc_slidingMeanRadioButton = new GridBagConstraints();
		gbc_slidingMeanRadioButton.anchor = GridBagConstraints.WEST;
		gbc_slidingMeanRadioButton.insets = new Insets(0, 0, 5, 0);
		gbc_slidingMeanRadioButton.gridx = 0;
		gbc_slidingMeanRadioButton.gridy = 3;
		dataControlPanel_.add(slidingMeanRadioButton_, gbc_slidingMeanRadioButton);

		showOrUpdatePlotButton_ = new JButton("Show/update plot");
		GridBagConstraints gbc_showOrUpdatePlotButton = new GridBagConstraints();
		gbc_showOrUpdatePlotButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_showOrUpdatePlotButton.insets = new Insets(0, 0, 5, 0);
		gbc_showOrUpdatePlotButton.gridx = 0;
		gbc_showOrUpdatePlotButton.gridy = 4;
		dataControlPanel_.add(showOrUpdatePlotButton_, gbc_showOrUpdatePlotButton);

		printTableButton_ = new JButton("Print table");
		GridBagConstraints gbc_printTableButton = new GridBagConstraints();
		gbc_printTableButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_printTableButton.insets = new Insets(0, 0, 5, 0);
		gbc_printTableButton.gridx = 0;
		gbc_printTableButton.gridy = 5;
		dataControlPanel_.add(printTableButton_, gbc_printTableButton);

		clearAllDataButton_ = new JButton("Clear all data");
		clearAllDataButton_.setToolTipText("Remove all selections but keep data loaded.");
		GridBagConstraints gbc_clearAllDataButton = new GridBagConstraints();
		gbc_clearAllDataButton.anchor = GridBagConstraints.SOUTHWEST;
		gbc_clearAllDataButton.insets = new Insets(0, 0, 5, 0);
		gbc_clearAllDataButton.gridx = 0;
		gbc_clearAllDataButton.gridy = 7;
		dataControlPanel_.add(clearAllDataButton_, gbc_clearAllDataButton);

		exitButton_ = new JButton("Exit");
		GridBagConstraints gbc_exitButton = new GridBagConstraints();
		gbc_exitButton.anchor = GridBagConstraints.NORTHWEST;
		gbc_exitButton.gridx = 0;
		gbc_exitButton.gridy = 9;
		dataControlPanel_.add(exitButton_, gbc_exitButton);
		tabbedPane_.addTab("Data input", dataInputPanel_);

		geneSelectionListReference_ = new JCheckBoxList<JCheckboxWithObject>();
		geneSelectionScrollPaneReference_ = new JScrollPane(geneSelectionListReference_);
		GridBagConstraints gbc_geneSelectionScrollPaneReference = new GridBagConstraints();
		gbc_geneSelectionScrollPaneReference.fill = GridBagConstraints.BOTH;
		gbc_geneSelectionScrollPaneReference.gridx = 2;
		gbc_geneSelectionScrollPaneReference.gridy = 1;
		dataInputPanel_.add(geneSelectionScrollPaneReference_, gbc_geneSelectionScrollPaneReference);

		menuBar_ = new JMenuBar();
		frame_.setJMenuBar(menuBar_);

		mnFileAnalysis_ = new JMenu("Analysis data");
		menuBar_.add(mnFileAnalysis_);

		mntmOpenAnalysisGenbank_ = new JMenuItem("load genbank file...");
		mnFileAnalysis_.add(mntmOpenAnalysisGenbank_);

		mntmOpenAnalysisFasta_ = new JMenuItem("load FASTA file...");
		mnFileAnalysis_.add(mntmOpenAnalysisFasta_);

		mntmPasteTextAnalysis_ = new JMenuItem("paste as text...");
		mnFileAnalysis_.add(mntmPasteTextAnalysis_);

		mnFileReference_ = new JMenu("Reference data");
		menuBar_.add(mnFileReference_);

		mntmOpenReferenceGenbank_ = new JMenuItem("load genbank file...");
		mnFileReference_.add(mntmOpenReferenceGenbank_);

		mntmOpenReferenceFasta_ = new JMenuItem("load FASTA file...");
		mnFileReference_.add(mntmOpenReferenceFasta_);

		mntmPasteTextReference_ = new JMenuItem("paste as text...");
		mnFileReference_.add(mntmPasteTextReference_);

		mnExport_ = new JMenu("Export");
		menuBar_.add(mnExport_);

		mnPrintTable_ = new JMenu("print table...");
		mnExport_.add(mnPrintTable_);

		mntmAllData_ = new JMenuItem("all data");
		mnPrintTable_.add(mntmAllData_);

		mntmSelectedCodons_ = new JMenuItem("selected codons...");
		mnPrintTable_.add(mntmSelectedCodons_);
	}

	private void initActionListeners() {
		selectAllAnalysisButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(selectAllAnalysisButton_)) {
					geneSelectionListAnalysis_.selectAll();
				}
			}
		});

		deselectAllAnalysisButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(deselectAllAnalysisButton_)) {
					geneSelectionListAnalysis_.deselectAll();
				}
			}
		});

		selectAllReferenceButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(selectAllReferenceButton_)) {
					geneSelectionListReference_.selectAll();
				}
			}
		});

		deselectAllReferenceButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(deselectAllReferenceButton_)) {
					geneSelectionListReference_.deselectAll();
				}
			}
		});

		showOrUpdatePlotButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cuAnalyzer_.collectSelectedSequencesForAnalysis(geneSelectionListAnalysis_);
				cuAnalyzer_.collectSelectedSequencesAsReference(geneSelectionListReference_);
				if (checkDataPresent()) {
					ColorChooserDialog dialog = new ColorChooserDialog(cuModel_);
					dialog.showDialog();
					refreshChart(true);
				}
			}
		});

		printTableButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cuAnalyzer_.collectSelectedSequencesForAnalysis(geneSelectionListAnalysis_);
				cuAnalyzer_.collectSelectedSequencesAsReference(geneSelectionListReference_);
				if (e.getSource().equals(printTableButton_)) {
					if (checkDataPresent()) {
						chooseFileForResultTable();
					}
				}
			}
		});

		clearAllDataButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(clearAllDataButton_)) {
					resetAllData();
				}
			}
		});

		scatterPlotRadioButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(scatterPlotRadioButton_)) {
					if (scatterPlotRadioButton_.isSelected()) {
						cuModel_.setChartType(ChartType.SCATTER_PLOT);
					}
				}
			}
		});

		barPlotRadioButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(barPlotRadioButton_)) {
					if (barPlotRadioButton_.isSelected()) {
						cuModel_.setChartType(ChartType.BAR_PLOT);
					}
				}
			}
		});

		slidingMeanRadioButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(slidingMeanRadioButton_)) {
					if (slidingMeanRadioButton_.isSelected()) {
						cuModel_.setChartType(ChartType.MOVING_AVERAGE);
					}
				}

			}
		});

		mntmOpenAnalysisGenbank_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(mntmOpenAnalysisGenbank_)) {
					loadAnalysisData(InputFileType.GENBANK);
				}
			}
		});

		mntmOpenAnalysisFasta_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(mntmOpenAnalysisFasta_)) {
					loadAnalysisData(InputFileType.FASTA);
				}

			}
		});

		mntmOpenReferenceGenbank_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(mntmOpenReferenceGenbank_)) {
					loadReferenceData(InputFileType.GENBANK);
				}
			}
		});

		mntmOpenReferenceFasta_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(mntmOpenReferenceFasta_)) {
					loadReferenceData(InputFileType.FASTA);
				}

			}
		});

		mntmPasteTextAnalysis_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(mntmPasteTextAnalysis_)) {
					fetchSequencePastedAsTextForAnalysis();
				}
			}
		});

		mntmPasteTextReference_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(mntmPasteTextAnalysis_)) {
					fetchSequencePastedAsTextAsReference();
				}
			}
		});

		mntmSelectedCodons_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				exportDataForSelectedCodons();
			}
		});

		mntmAllData_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseFileForResultTable();
			}
		});

		exitButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
	}

	private Boolean checkDataPresent() {
		String errorMessage = "Please choose any ";
		Boolean analysisDataPresent = checkAnalysisDataPresent();
		Boolean referenceDataPresent = checkReferenceDataPresent();
		if (analysisDataPresent == true && referenceDataPresent == false) {
			errorMessage += "reference data!";
		} else if (analysisDataPresent == false && referenceDataPresent == true) {
			errorMessage += "analysis data!";
		} else if (analysisDataPresent == false && referenceDataPresent == false) {
			errorMessage += "analyis data and reference data!";
		} else {
			return true;
		}
		JOptionPane.showMessageDialog(null, errorMessage);
		return false;
	}

	private Boolean checkAnalysisDataPresent() {
		if (cuModel_.getSequencesForAnalysis().size() == 0) {
			return false;
		} else {
			return true;
		}
	}

	private Boolean checkReferenceDataPresent() {
		if (cuModel_.getSequencesAsReference().size() == 0) {
			return false;
		} else {
			return true;
		}
	}

	private void resetAllData() {
		cuModel_.resetCalculationData();
		cuModel_.resetLoadedRawData();
		if (tabbedPane_.getTabCount() > 1 && tabbedPane_.getComponentAt(1) != null) {
			tabbedPane_.removeTabAt(1);
		}
		geneSelectionListAnalysis_.deselectAll();
		geneSelectionListReference_.deselectAll();
	}
}
