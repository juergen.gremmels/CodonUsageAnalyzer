/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.ui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.swing.Box;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.text.NumberFormatter;

import mpimp.codonusage.db.CodonUsageModel;
import mpimp.codonusage.util.CUAnalyzer;

public class SearchPanel extends JPanel {

	private static final long serialVersionUID = 4021887780984848914L;
	private JTextField searchStringTextField_;
	private JTextField positonsTextField_;
	private JLabel caiLabel_;
	private MainWindow mainWindow_;
	private JLabel caiDescriptionLabel_;
	private JLabel searchPatternLabel_;
	private JLabel searchResultLabel_;
	private JButton saveSearchResultButton_;
	private JButton refreshButton_;
	private JCheckBox useTripletFrameCheckBox_;

	private CodonUsageModel cuModel_;
	private JFormattedTextField leftBoundaryTextField_;
	private JFormattedTextField rightBoundaryTextField_;
	/**
	 * @wbp.nonvisual location=97,29
	 */
	private final Box horizontalBox = Box.createHorizontalBox();

	/**
	 * Create the panel.
	 */
	public SearchPanel(MainWindow mainWindow, CodonUsageModel cuModel) {
		mainWindow_ = mainWindow;
		cuModel_ = cuModel;

		caiLabel_ = new JLabel("CAI");
		caiLabel_.setOpaque(true);
		caiLabel_.setBackground(Color.WHITE);
		searchStringTextField_ = new JTextField();
		searchStringTextField_.setColumns(10);
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		dfs.setPerMill(',');
		final DecimalFormat df = new DecimalFormat("#.##", dfs);
		caiLabel_.setText(df.format(cuModel_.getCodonAdaptationIndex()));

		refreshButton_ = new JButton("refresh");

		refreshButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(refreshButton_)) {
					if (validateInput() == true) {
						NumberFormat deFormat = NumberFormat.getNumberInstance(Locale.GERMAN);
						cuModel_.setSearchString(searchStringTextField_.getText());
						try {
							cuModel_.setRightBoundary(deFormat.parse(rightBoundaryTextField_.getText()).intValue());
							cuModel_.setLeftBoundary(deFormat.parse(leftBoundaryTextField_.getText()).intValue());
						} catch (ParseException e1) {
							e1.printStackTrace();
							return;
						}
						mainWindow_.refreshSearchResults();
						caiLabel_.setText(df.format(cuModel_.getCodonAdaptationIndex()));
						positonsTextField_.setText(createDisplayPositionsString());
					}
				}
			}
		});

		saveSearchResultButton_ = new JButton("save");
		
		saveSearchResultButton_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(saveSearchResultButton_)) {
					CUAnalyzer cuAnalyzer = new CUAnalyzer(cuModel_);
					cuAnalyzer.exportSearchResults(mainWindow_.getFrame());
				}
			}
		});
		
		positonsTextField_ = new JTextField();
		positonsTextField_.setBackground(Color.WHITE);
		positonsTextField_.setEditable(false);

		caiDescriptionLabel_ = new JLabel("CAI: ");

		searchPatternLabel_ = new JLabel("Search pattern:");

		searchResultLabel_ = new JLabel("Search result:");

		useTripletFrameCheckBox_ = new JCheckBox("use triplet frame");
		useTripletFrameCheckBox_.setSelected(true);
		useTripletFrameCheckBox_.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(useTripletFrameCheckBox_)) {
					cuModel_.setUseTripletFrame(useTripletFrameCheckBox_.isSelected());
				}
			}
		});

		JLabel leftBoundaryDescLabel_ = new JLabel("Left boundary:");

		NumberFormat format = NumberFormat.getInstance();
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(0);
		formatter.setMaximum(Integer.MAX_VALUE);
		formatter.setAllowsInvalid(false);

		leftBoundaryTextField_ = new JFormattedTextField(formatter);
		leftBoundaryTextField_.setColumns(10);
		leftBoundaryTextField_.setText(cuModel_.getLeftBoundary().toString());

		JLabel rightBoundaryDescLabel_ = new JLabel("Right boundary:");

		rightBoundaryTextField_ = new JFormattedTextField(formatter);
		rightBoundaryTextField_.setColumns(10);
		rightBoundaryTextField_.setText(cuModel_.getRightBoundary().toString());

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(caiDescriptionLabel_)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(caiLabel_)
					.addGap(18)
					.addComponent(searchPatternLabel_)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(searchStringTextField_, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(useTripletFrameCheckBox_)
					.addGap(6)
					.addComponent(refreshButton_)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(searchResultLabel_)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(positonsTextField_, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(saveSearchResultButton_)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(leftBoundaryDescLabel_)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(leftBoundaryTextField_, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
					.addComponent(rightBoundaryDescLabel_)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(rightBoundaryTextField_, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(12)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(caiLabel_)
						.addComponent(caiDescriptionLabel_)))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(searchStringTextField_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(searchPatternLabel_)
						.addComponent(useTripletFrameCheckBox_)))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(7)
					.addComponent(refreshButton_))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(9)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(positonsTextField_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(searchResultLabel_)
						.addComponent(leftBoundaryDescLabel_)
						.addComponent(leftBoundaryTextField_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(rightBoundaryDescLabel_)
						.addComponent(rightBoundaryTextField_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(saveSearchResultButton_)))
		);
		setLayout(groupLayout);

	}

	private boolean validateInput() {
		NumberFormat deFormat = NumberFormat.getNumberInstance(Locale.GERMAN);
		Integer leftBoundary;
		Integer rightBoundary;
		try {
			leftBoundary = deFormat.parse(leftBoundaryTextField_.getText()).intValue();
			rightBoundary = deFormat.parse(rightBoundaryTextField_.getText()).intValue();
			if (leftBoundary > rightBoundary) {
				return false;
			}
			return true;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	private String createDisplayPositionsString() {
		String displayPositions = "";
		if (cuModel_.getSearchPatternPositionsMap() != null) {
			Set<String> searchPatterns = cuModel_.getSearchPatternPositionsMap().keySet();
			for (String currentSearchPattern : searchPatterns) {
				List<Integer> positionsForCurrentPattern = cuModel_.getSearchPatternPositionsMap()
						.get(currentSearchPattern);
				displayPositions += currentSearchPattern + ": ";
				int counter = 0;
				for (Integer currentPosition : positionsForCurrentPattern) {
					counter++;
					displayPositions += currentPosition;
					if (counter < positionsForCurrentPattern.size()) {
						displayPositions += ",";
					} else {
						displayPositions += "; ";
					}
				}
			}
		}
		return displayPositions;
	}

	public JCheckBox getUseTripletFrameCheckBox() {
		return useTripletFrameCheckBox_;
	}
	
	public JFormattedTextField getLeftBoundaryTextField() {
		return leftBoundaryTextField_;
	}

	public JFormattedTextField getRightBoundaryTextField() {
		return rightBoundaryTextField_;
	}
}
