/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import mpimp.codonusage.db.CodonUsageModel;

public class SelectCodonsForExportDialog extends JDialog {

	private static final long serialVersionUID = -2617181831649058272L;
	private final JPanel contentPanel_ = new JPanel();
	private CodonUsageModel cuModel_;
	private JTextField selectedCodonsTextField_;
	private Boolean exportTable_ = false;

	public Boolean showDialog() {
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setModal(true);
		this.setVisible(true);
		return exportTable_;
	}

	/**
	 * Create the dialog.
	 */
	public SelectCodonsForExportDialog(Window owner, CodonUsageModel cuModel) {
		super(owner);
		cuModel_ = cuModel;
		setBounds(100, 100, 450, 187);
		getContentPane().setLayout(new BorderLayout());
		contentPanel_.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel_, BorderLayout.CENTER);

		selectedCodonsTextField_ = new JTextField();
		selectedCodonsTextField_.setColumns(10);

		JLabel lblPleaseEnterA = new JLabel("Please enter a comma separated list of codons:");

		JLabel lblYouMaySpecify = new JLabel("You may specify here for which codon(s) data will be exported.");
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel_);
		gl_contentPanel.setHorizontalGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup().addContainerGap()
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(selectedCodonsTextField_, GroupLayout.PREFERRED_SIZE, 394,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblPleaseEnterA).addComponent(lblYouMaySpecify))
						.addContainerGap(20, Short.MAX_VALUE)));
		gl_contentPanel.setVerticalGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPanel.createSequentialGroup()
						.addContainerGap(71, Short.MAX_VALUE).addComponent(lblYouMaySpecify).addGap(18)
						.addComponent(lblPleaseEnterA).addGap(18).addComponent(selectedCodonsTextField_,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(64)));
		contentPanel_.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);

				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						makeCodonList();
						exportTable_ = true;
						setVisible(false);
						dispose();
					}
				});

			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);

				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						exportTable_ = false;
						setVisible(false);
						dispose();
					}
				});
			}
		}
	}

	private void makeCodonList() {
		List<String> selectedCodonsList = new ArrayList<String>();
		String codonInput = selectedCodonsTextField_.getText();
		String[] codons = codonInput.split("[,;\\.]");
		for (int i = 0; i < codons.length; i++) {
			String codon = codons[i];
			codon = codon.trim();
			codon = codon.toUpperCase();
			if (codon.length() != 3) {
				continue;
			}
			selectedCodonsList.add(codon);
		}
		if (cuModel_ != null) {
			cuModel_.setSelectedCodonsForExport(selectedCodonsList);
		}
	}
}
