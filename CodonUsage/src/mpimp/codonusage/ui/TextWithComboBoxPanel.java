/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.ui;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import mpimp.codonusage.db.AnnotationRecord;
import mpimp.codonusage.db.ColorComboBoxModel;
import java.awt.Dimension;

public class TextWithComboBoxPanel extends JPanel implements Observer {

	/**
	 * Create the panel.
	 */
	public TextWithComboBoxPanel(AnnotationRecord annotationRecord) {
		setSize(new Dimension(0, 30));

		annotationRecord_ = annotationRecord;
		selectedSequenceLabel_ = new JLabel(annotationRecord_.toString());

		colorComboBox_ = new ColorComboBox();
		colorComboBox_.setModel(new ColorComboBoxModel());
		colorComboBox_.setRenderer(new ColorComboBoxRenderer());
		fillColorList();
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout
				.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addContainerGap()
								.addComponent(selectedSequenceLabel_).addGap(18)
								.addComponent(colorComboBox_, GroupLayout.PREFERRED_SIZE, 67,
										GroupLayout.PREFERRED_SIZE)
				.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup().addContainerGap()
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(selectedSequenceLabel_)
						.addComponent(colorComboBox_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE))
				.addContainerGap(9, Short.MAX_VALUE)));
		setLayout(groupLayout);

	}

	@Override
	public void update(Observable o, Object arg) {
		ColorComboBoxModel model = (ColorComboBoxModel) ((ColorComboBox) colorComboBox_).getModel();
		annotationRecord_.setDisplayColor((Color) model.getSelectedItem());
	}

	public void setSelectedItem(int index) {
		if (annotationRecord_.getDisplayColor() != null) {
			colorComboBox_.setSelectedItem(annotationRecord_.getDisplayColor());
		} else if (colorComboBox_.getItemCount() > index) {
			colorComboBox_.setSelectedIndex(index);
		} else {
			Integer itemCount = colorComboBox_.getItemCount();
			Integer factor = index/itemCount;
			Integer realIndex = index - factor * itemCount;
			colorComboBox_.setSelectedIndex(realIndex);
		}
	}

	private void fillColorList() {
		ColorComboBoxModel model = (ColorComboBoxModel) colorComboBox_.getModel();
		model.addElement(Color.RED);
		model.addElement(Color.GREEN);
		model.addElement(Color.MAGENTA);
		model.addElement(Color.YELLOW);
		model.addElement(Color.ORANGE);
		model.addElement(Color.GRAY);
		model.addElement(Color.PINK);
		model.addElement(Color.BLACK);
		model.addElement(Color.CYAN);
		model.addElement(Color.DARK_GRAY);
	}

	private JLabel selectedSequenceLabel_;
	private JComboBox<Color> colorComboBox_;
	private AnnotationRecord annotationRecord_;
}
