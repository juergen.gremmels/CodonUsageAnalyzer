/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ListModel;

import mpimp.codonusage.db.AnnotationRecord;
import mpimp.codonusage.db.CodonList;
import mpimp.codonusage.db.CodonRecord;
import mpimp.codonusage.db.CodonUsageModel;
import mpimp.codonusage.ui.JCheckBoxList;
import mpimp.codonusage.ui.JCheckboxWithObject;
import mpimp.codonusage.ui.MainWindow.InputFileType;

public class CUAnalyzer {

	public CUAnalyzer(CodonUsageModel cuModel) {
		cuModel_ = cuModel;
	}

	public Map<String, List<Integer>> getSearchPatternPositionsMap() {
		Map<String, List<Integer>> searchPatternPositionsMap = new HashMap<String, List<Integer>>();

		if (cuModel_.getSearchPatternList() != null) {
			for (String currentSearchPattern : cuModel_.getSearchPatternList()) {
				searchPatternPositionsMap.put(currentSearchPattern, getSearchPatternPositions(currentSearchPattern));
			}
		}
		return searchPatternPositionsMap;
	}

	public void printTable() throws IOException {
		refreshBasicCalculation();
		CodonUsageTableCalculator cutc = new CodonUsageTableCalculator(cuModel_);
		cutc.printCodonUsageTable();
	}

	public void exportSearchResults(JFrame parent) {
		JFileChooser fileChooser = new JFileChooser();
		int retVal = fileChooser.showSaveDialog(parent);
		if (retVal == JFileChooser.APPROVE_OPTION) {
			File info = new File(fileChooser.getSelectedFile().getAbsolutePath());
			if (info.exists()) {
				if (JOptionPane.showConfirmDialog(parent, "Overwrite?", "File exists!",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					try {
						info.delete();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				} else {
					return;
				}
			}
			cuModel_.setSearchResultFilePath(fileChooser.getSelectedFile().getAbsolutePath());
			try {
				saveSearchResults(writeSearchResults());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	private List<String> writeSearchResults() {
		List<String> fileContent = new ArrayList<String>();
		String line = "Search results";
		fileContent.add(line);
		line = "<codon(s)>: <position in analysis sequence>";
		if (cuModel_.getUseTripletFrame()) {
			line += ";<position of first codon in genome>";
		}
		fileContent.add(line);
		if (cuModel_.getSearchPatternPositionsMap() != null) {
			Set<String> searchPatterns = cuModel_.getSearchPatternPositionsMap().keySet();
			for (String currentSearchPattern : searchPatterns) {
				List<Integer> positionsForCurrentPattern = cuModel_.getSearchPatternPositionsMap()
						.get(currentSearchPattern);
				line = currentSearchPattern + ": ";
				int counter = 0;
				for (Integer currentPosition : positionsForCurrentPattern) {
					counter++;
					line += currentPosition;
					if (cuModel_.getUseTripletFrame()) {
						// position in genome not yet available when searching
						// without triplet frame
						Integer positionInGenome = cuModel_.getCodonCollectionForAnalysisPosInAnalysisSequence()
								.get(currentPosition).getPositionInGenome();
						line += "(" + positionInGenome + ")";
					}
					if (counter < positionsForCurrentPattern.size()) {
						line += ",";
					}
				}
				fileContent.add(line);
			}
		}
		return fileContent;
	}

	private void saveSearchResults(List<String> fileContent) throws IOException {
		Path path = Paths.get(cuModel_.getSearchResultFilePath());
		Files.write(path, fileContent, StandardCharsets.UTF_8);
	}

	public void refreshBasicCalculation() {
		CodonList codonList = new CodonList(cuModel_.getSequenceForAnalysis());

		cuModel_.setPositions(codonList.getPositionMapBaseCount());

		if (cuModel_.getLeftBoundary() == -1 || cuModel_.getRightBoundary() == -1) {
			// boundaries have not yet been set
			cuModel_.setLeftBoundary(1);
			cuModel_.setRightBoundary(
					(Integer) ((TreeMap<Integer, String>) codonList.getPositionMapBaseCount()).lastKey());
		}
		CodonUsageTableCalculator cutc = new CodonUsageTableCalculator(cuModel_);
		cutc.collectCodons();
		cutc.calculateCodonUsage();
		cuModel_.setCodonAdaptationIndex(cutc.calculateCodonAdaptationIndex(cuModel_.getCurCollectionForAnalysis()));
	}

	public void readEntireSequenceFromInputFile(InputFileType inputFileType) throws IOException {
		if (inputFileType == InputFileType.GENBANK) {
			cuModel_.setWholeGenomeSequenceForAnalysis(
					SequenceFileParser.extractSequenceFromGenebankFile(cuModel_.getSourceFilePath()));
		} else if (inputFileType == InputFileType.FASTA) {
			cuModel_.setWholeGenomeSequenceForAnalysis(
					SequenceFileParser.extractSequenceFromFastaFile(cuModel_.getSourceFilePath()));
		}
	}

	public void readEntireSequenceFromInputFileAsReference(InputFileType inputFileType) throws IOException {
		if (inputFileType == InputFileType.GENBANK) {
			cuModel_.setWholeGenomeSequenceAsReference(
					SequenceFileParser.extractSequenceFromGenebankFile(cuModel_.getSourceFilePath()));
		} else if (inputFileType == InputFileType.FASTA) {
			cuModel_.setWholeGenomeSequenceAsReference(
					SequenceFileParser.extractSequenceFromFastaFile(cuModel_.getSourceFilePath()));
		}
	}

	public void collectSelectedSequencesForAnalysis(JCheckBoxList geneSelectionList) {
		collectSequencesForAnalysis(geneSelectionList, true);
	}

	public void collectAllButSelectedSequencesForAnalysis(JCheckBoxList geneSelectionList) {
		collectSequencesForAnalysis(geneSelectionList, false);
	}

	public void collectSelectedSequencesAsReference(JCheckBoxList geneSelectionList) {
		collectSequencesAsReference(geneSelectionList, true);
	}

	public void collectAllButSelectedSequencesAsReference(JCheckBoxList geneSelectionList) {
		collectSequencesAsReference(geneSelectionList, false);
	}

	private void collectSequencesForAnalysis(JCheckBoxList geneSelectionList, Boolean useSelected) {
		ListModel model = geneSelectionList.getModel();
		cuModel_.setSequencesForAnalysis(new ArrayList<AnnotationRecord>());
		cuModel_.setSequenceForAnalysis("");
		for (int i = 0; i < model.getSize(); i++) {
			JCheckboxWithObject item = (JCheckboxWithObject) model.getElementAt(i);
			if (item.isSelected() == useSelected) {
				if (item.getObject() instanceof AnnotationRecord) {
					AnnotationRecord currentRecord = (AnnotationRecord) item.getObject();
					String sequence = cuModel_.getSequenceForAnalysis();
					cuModel_.setSequenceForAnalysis(sequence += currentRecord.getPartSequence());
					cuModel_.getSequencesForAnalysis().add(currentRecord);
				}
			}
		}
	}

	private void collectSequencesAsReference(JCheckBoxList geneSelectionList, Boolean useSelected) {
		ListModel model = geneSelectionList.getModel();
		cuModel_.setSequencesAsReference(new ArrayList<AnnotationRecord>());
		cuModel_.setSequenceAsReference("");
		for (int i = 0; i < model.getSize(); i++) {
			JCheckboxWithObject item = (JCheckboxWithObject) model.getElementAt(i);
			if (item.isSelected() == useSelected) {
				if (item.getObject() instanceof AnnotationRecord) {
					AnnotationRecord currentRecord = (AnnotationRecord) item.getObject();
					String sequence = cuModel_.getSequenceAsReference();
					cuModel_.setSequenceAsReference(sequence += currentRecord.getPartSequence());
					cuModel_.getSequencesAsReference().add(currentRecord);
				}
			}
		}
	}

	private List<Integer> getSearchPatternPositionsFreeText(String searchPattern) {
		searchPattern = searchPattern.toUpperCase();
		List<Integer> positions = new ArrayList<Integer>();
		String sequence = cuModel_.getSequenceForAnalysis();
		Pattern pattern = Pattern.compile(searchPattern);
		Matcher matcher = pattern.matcher(sequence);
		while (matcher.find()) {
			Integer position = matcher.start() + 1;
			if (position >= cuModel_.getLeftBoundary() && position <= cuModel_.getRightBoundary()) {
				positions.add(position);
			}
		}
		return positions;
	}

	private List<Integer> getSearchPatternPositions(String searchPattern) {
		List<Integer> positions = new ArrayList<Integer>();
		String trimmedSearchPattern = SequenceCalculator.trimStringToTripletString(searchPattern);
		if (!searchPattern.equals("")) {
			if (cuModel_.getUseTripletFrame() == true) {
				CodonList codonList = new CodonList(cuModel_.getSequenceForAnalysis());
				positions = codonList.getPositionsOfCodonGroupBaseCount(trimmedSearchPattern,
						cuModel_.getLeftBoundary(), cuModel_.getRightBoundary());
			} else {
				positions = getSearchPatternPositionsFreeText(searchPattern);
			}
		}
		return positions;
	}

	private CodonUsageModel cuModel_;

}
