/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import mpimp.codonusage.db.AnnotationRecord;
import mpimp.codonusage.db.CodonRecord;
import mpimp.codonusage.db.CodonUsageModel;
import mpimp.codonusage.db.CodonUsageRecord;
import mpimp.codonusage.db.GeneticCode;
import mpimp.codonusage.poi.CUWorkbook;
import mpimp.codonusage.poi.CUWorksheet;

public class CodonUsageTableCalculator {

	public CodonUsageTableCalculator(CodonUsageModel cuModel) {
		cuModel_ = cuModel;
	}

	public void calculateCodonUsage() {
		Map<String, CodonUsageRecord> codonCollectionForAnalysis = cuModel_.getCurCollectionForAnalysis();
		Map<String, CodonUsageRecord> codonCollectionAsReference_ = cuModel_.getCurCollectionAsReference();
		for (String aminoAcid : GeneticCode.getAminoAcidCodonMap().keySet()) {
			// collect all codons for the current amino acid
			List<CodonUsageRecord> recordsForAAForAnalysis = new ArrayList<CodonUsageRecord>();
			Integer overallCodonFrequencyPerAAForAnalysis = 0;
			Integer overallCodonFrequencyPerAAAsReference = 0;

			Integer noOfMostFrequentAAAnalysis = 0;
			
			for (String currentCodon : GeneticCode.getAminoAcidCodonMap().get(aminoAcid)) {
				CodonUsageRecord currentRecord = codonCollectionForAnalysis.get(currentCodon);
				if (currentRecord != null) {
					Integer currentFrequency = currentRecord.getFrequency();
					// sum up frequencies and collect all records for the
					// current amino acid in a separate list
					overallCodonFrequencyPerAAForAnalysis += currentFrequency;
					recordsForAAForAnalysis.add(currentRecord);
					if (currentFrequency > noOfMostFrequentAAAnalysis) {
						noOfMostFrequentAAAnalysis = currentFrequency;
					}
				}
			}

			Integer noOfMostFrequentAAReference = 0;
			
			for (String currentCodon : GeneticCode.getAminoAcidCodonMap().get(aminoAcid)) {
				CodonUsageRecord currentRecord = codonCollectionAsReference_.get(currentCodon);
				if (currentRecord != null) {
					Integer currentFrequency = currentRecord.getFrequency();
					// sum up frequencies
					overallCodonFrequencyPerAAAsReference += currentFrequency;
					// recordsForAAAsReference.add(currentRecord);
					if (currentFrequency > noOfMostFrequentAAReference) {
						noOfMostFrequentAAReference = currentFrequency;
					}
				}
			}

			// now calculate the fraction - for this we need the overall
			// frequency calculated just before
			Double maxFractionAnalysisAnalysis = 0.0;
			for (CodonUsageRecord currentRecord : recordsForAAForAnalysis) {
				Double fraction = currentRecord.getFrequency().doubleValue()
						/ overallCodonFrequencyPerAAForAnalysis.doubleValue();
				codonCollectionForAnalysis.get(currentRecord.getCodon()).setFractionRelToAnalysisSequence(fraction);
				if (fraction > maxFractionAnalysisAnalysis) {
					maxFractionAnalysisAnalysis = fraction;
				}
			}

			Double maxFractionAnalysisReference = 0.0;
			for (CodonUsageRecord currentRecord : recordsForAAForAnalysis) {
				Double fraction = currentRecord.getFrequency().doubleValue()
						/ overallCodonFrequencyPerAAAsReference.doubleValue();
				codonCollectionForAnalysis.get(currentRecord.getCodon()).setFractionRelToReferenceSequence(fraction);
				if (fraction > maxFractionAnalysisReference) {
					maxFractionAnalysisReference = fraction;
				}
			}

			Integer analysisSequenceLength = calculateRealAnalysisSequenceLength();

			// now calculate the relative adaptiveness
			for (CodonUsageRecord currentRecord : recordsForAAForAnalysis) {
				Double relativeAdaptivenessRelToAnalysis = currentRecord.getFrequency().doubleValue()
						/ noOfMostFrequentAAAnalysis.doubleValue();
				currentRecord.setAdaptivenessRelToAnalysisSequence(relativeAdaptivenessRelToAnalysis);
				Double relativeAdaptivenessRelToAnalysisNorm = relativeAdaptivenessRelToAnalysis
						/ analysisSequenceLength;
				currentRecord.setAdaptivenessRelToAnalysisSequenceNorm(relativeAdaptivenessRelToAnalysisNorm);
				Double relativeAdaptivenessRelToReference = currentRecord.getFrequency().doubleValue()
						/ noOfMostFrequentAAReference.doubleValue();
				currentRecord.setAdaptivenessRelToReferenceSequence(relativeAdaptivenessRelToReference);
				Double relativeAdaptivenessRelToReferenceNorm = relativeAdaptivenessRelToReference
						/ analysisSequenceLength;
				currentRecord.setAdaptivenessRelToReferenceSequenceNorm(relativeAdaptivenessRelToReferenceNorm);
			}
		}

	}

	public void printCodonUsageTable() throws IOException {
		List<List<Object>> resultTableContent = new ArrayList<List<Object>>();

		List<String> selectedCodonsForExport = cuModel_.getSelectedCodonsForExport();
		Boolean filterCodons = false;
		if (selectedCodonsForExport != null && selectedCodonsForExport.size() > 0) {
			filterCodons = true;
		}

		// int wholeSequenceLength = cuModel_.getWholeGenomeSequence().length();
		int wholeSequenceLength = cuModel_.getWholeGenomeSequenceAsReference().length();

		CodonRecord currentCodon = null;

		for (Integer i = 1; i <= wholeSequenceLength - 2; i++) {
			currentCodon = cuModel_.getCodonCollectionForAnalysis().get(i);
			if (currentCodon != null) {
				if (filterCodons == true && selectedCodonsForExport.contains(currentCodon.getCodon()) == false) {
					continue;
				}
				List<Object> resultLine = new ArrayList<Object>();
				resultLine.add(currentCodon.getPositionInGenome());
				resultLine.add(currentCodon.getCodon());
				resultLine.add(currentCodon.getAminoAcid());
				resultLine.add(currentCodon.getAnnotationOfSourceSequence());
				resultLine.add(currentCodon.getCodonUsageRecord().getFrequency());
				resultLine.add(currentCodon.getCodonUsageRecord().getFractionRelToAnalysisSequence());
				resultLine.add(currentCodon.getCodonUsageRecord().getFractionRelToReferenceSequence());
				resultLine.add(currentCodon.getCodonUsageRecord().getAdaptivenessRelToAnalysisSequence());
				resultLine.add(currentCodon.getCodonUsageRecord().getAdaptivenessRelToReferenceSequence());
				resultLine.add(currentCodon.getCodonUsageRecord().getAdaptivenessRelToAnalysisSequenceNorm());
				resultLine.add(currentCodon.getCodonUsageRecord().getAdaptivenessRelToReferenceSequenceNorm());
				resultLine.add(currentCodon.getPositionInAnalysisSequence());
				resultLine.add(currentCodon.getPositionInPartSequence());
				resultTableContent.add(resultLine);
			}
		}

		List<String> header = new ArrayList<String>();
		header.add("codon position");
		header.add("codon");
		header.add("AA");
		header.add("annotation");
		header.add("abs. frequency");
		header.add("frequency/sequence");
		header.add("frequency/reference");
		header.add("rel. adaptiveness/sequence");
		header.add("rel. adaptiveness/reference");
		header.add("rel. adaptiveness/sequence (norm.)");
		header.add("rel. adaptiveness/reference (norm.)");
		header.add("position in sequence");
		header.add("position in gene");

		CUWorkbook workbook = new CUWorkbook();
		CUWorksheet worksheet = workbook.createCUWorksheet(header);
		worksheet.writeHeader();
		worksheet.writeBody(resultTableContent);
		workbook.write(cuModel_.getTableFilePath());
	}

	private Integer calculateRealAnalysisSequenceLength() {
		@SuppressWarnings("rawtypes")
		Integer maxValue = (Integer) ((TreeMap) cuModel_.getPositions()).lastKey();
		@SuppressWarnings("rawtypes")
		Integer minValue = (Integer) ((TreeMap) cuModel_.getPositions()).firstKey();
		Integer leftBoundary = cuModel_.getLeftBoundary();
		Integer rightBoundary = cuModel_.getRightBoundary();

		if (maxValue == rightBoundary && minValue == leftBoundary) {
			return cuModel_.getSequenceForAnalysis().length();
		} else if (leftBoundary > minValue && rightBoundary < maxValue) {
			return rightBoundary - leftBoundary;
		} else {
			return maxValue - leftBoundary;
		}
	}

	private void collectCodonsForAnalysis() {
		collectCodons(cuModel_.getSequencesForAnalysis(), cuModel_.getCurCollectionForAnalysis(),
				cuModel_.getCodonCollectionForAnalysis(), cuModel_.getCodonCollectionForAnalysisPosInAnalysisSequence(),
				true);
	}

	private void collectCodonsAsReference() {
		collectCodons(cuModel_.getSequencesAsReference(), cuModel_.getCurCollectionAsReference(),
				cuModel_.getCodonCollectionAsReference(), null, false);
	}

	public void collectCodons() {
		collectCodonsForAnalysis();
		collectCodonsAsReference();
	}

	private void collectCodons(List<AnnotationRecord> sequencesList, Map<String, CodonUsageRecord> curCollection,
			Map<Integer, CodonRecord> codonCollection, Map<Integer, CodonRecord> codonCollectionLocalPos,
			Boolean useBoundaries) {
		List<CodonRecord> codonsOfCurrentSequence = SequenceCalculator.splitSequenceIntoCodons(sequencesList);
		codonCollection.clear();
		curCollection.clear();
		if (codonCollectionLocalPos != null) {
			codonCollectionLocalPos.clear();
		}
		for (CodonRecord currentCodonRecord : codonsOfCurrentSequence) {
			if (useBoundaries && (currentCodonRecord.getPositionInAnalysisSequence() < cuModel_.getLeftBoundary()
					|| currentCodonRecord.getPositionInAnalysisSequence() > cuModel_.getRightBoundary())) {
				continue;
			}
			String aminoAcid = GeneticCode.getCodonAminoAcidMap().get(currentCodonRecord.getCodon());
			currentCodonRecord.setAminoAcid(aminoAcid);
			if (curCollection.containsKey(currentCodonRecord.getCodon())) {
				CodonUsageRecord currentCURecord = curCollection.get(currentCodonRecord.getCodon());
				Integer count = currentCURecord.getFrequency();
				count++;
				curCollection.get(currentCodonRecord.getCodon()).setFrequency(count);
				currentCodonRecord.setCodonUsageRecord(currentCURecord);
			} else {
				CodonUsageRecord currentCURecord = new CodonUsageRecord(aminoAcid, currentCodonRecord.getCodon(), 1);
				curCollection.put(currentCodonRecord.getCodon(), currentCURecord);
				currentCodonRecord.setCodonUsageRecord(currentCURecord);
			}
			codonCollection.put(currentCodonRecord.getPositionInGenome(), currentCodonRecord);
			if (codonCollectionLocalPos != null) {
				codonCollectionLocalPos.put(currentCodonRecord.getPositionInAnalysisSequence(), currentCodonRecord);
			}
		}
	}

	public Double calculateCodonAdaptationIndex(Map<String, CodonUsageRecord> codonCollection) {
		Double relAdSum = 0.0;
		for (CodonUsageRecord currentRecord : codonCollection.values()) {
			Double lnRelAd = Math.log(currentRecord.getAdaptivenessRelToAnalysisSequence());
			relAdSum += lnRelAd;
		}
		Double cai = Math.exp(relAdSum / codonCollection.size());
		return cai;
	}

	private CodonUsageModel cuModel_;

}
