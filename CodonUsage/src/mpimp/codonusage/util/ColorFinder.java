/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.util;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import mpimp.codonusage.db.CodonUsageModel;
import mpimp.codonusage.ui.CUXYSeries;

public class ColorFinder {

	public ColorFinder(CodonUsageModel cuModel) {
		cuModel_ = cuModel;
	}

	public Color getColor() {
		List<Color> currentColors = new ArrayList<Color>();
		for (CUXYSeries currentSeries : cuModel_.getDataSeriesMap().values()) {
			currentColors.add(currentSeries.getDisplayColor());
		}
		currentColors.addAll(cuModel_.getColorsForSearchPatternMap().values());
		currentColors.add(Color.BLUE);//this is the color for marking selected data points

		Color color = new Color((int) (Math.random() * 0x1000000));

		while (currentColors.contains(color)) {
			color = new Color((int) (Math.random() * 0x1000000));
		}
		return color;
	}

	private CodonUsageModel cuModel_;

}
