/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mpimp.codonusage.db.AnnotationRecord;
import mpimp.codonusage.db.JoinPartItem;

public class FastaAnnotationParser {

	public static List<AnnotationRecord> parseAnnotations(String filePath) throws Exception {
		List<AnnotationRecord> annotationRecords = new ArrayList<AnnotationRecord>();
		
		BufferedReader bufferedReader = null;
		
		Pattern annotationNameLinePattern = Pattern.compile(">.+");
		Pattern sequenceLinePattern = Pattern.compile("[AaGgCcTt]+");
		
		AnnotationRecord annotationRecord = null;
		
		String line = "";
		
		try {
			bufferedReader = new BufferedReader(new FileReader(filePath));
			while ((line = bufferedReader.readLine()) != null) {
				Matcher annotationNameLineMatcher = annotationNameLinePattern.matcher(line);
				Matcher sequenceLineMatcher = sequenceLinePattern.matcher(line);
				
				if (annotationNameLineMatcher.matches()) {
					line = line.substring(1);
					annotationRecord = new AnnotationRecord(line, false);
					annotationRecord.getQualifiers().add(line);
				} else if (sequenceLineMatcher.matches()) {
					line = line.trim();
					line = line.toUpperCase();
					if (annotationRecord != null) {
						annotationRecord.setPartSequence(line);
					}
					JoinPartItem jpi = new JoinPartItem();
					jpi.setSequence(line);
					jpi.setSequenceOriginal(line);
					annotationRecord.getJoinPartItems().add(jpi);
					annotationRecords.add(annotationRecord);
				}
			}
		} catch (Exception e) {
			String message = "Error parsing annotations from " + filePath + ". " + e.getMessage();
			throw new Exception(message);
		}
		if (bufferedReader != null) {
			bufferedReader.close();
		}
		
		return annotationRecords;
	}
	
}
