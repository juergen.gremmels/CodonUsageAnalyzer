/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mpimp.codonusage.db.AnnotationRecord;
import mpimp.codonusage.db.JoinPartItem;

public class GenbankAnnotationParser {

	public static List<AnnotationRecord> parseAnnotations(String filePath) throws Exception {
		String rawSequence = SequenceFileParser.extractSequenceFromGenebankFile(filePath);
		rawSequence = rawSequence.toUpperCase();

		BufferedReader bufferedReader = null;

		Pattern annotationNameLinePattern = Pattern.compile("^( {5})([a-zA-Z]{1,15})( +)([a-z0-9\\(\\)\\.,]+)");
		Pattern qualifierPattern = Pattern.compile("^( {21})(/.+)");

		List<AnnotationRecord> annotationRecords = new ArrayList<AnnotationRecord>();
		String line = "";

		AnnotationRecord annotationRecord = null;

		try {
			bufferedReader = new BufferedReader(new FileReader(filePath));
			while ((line = bufferedReader.readLine()) != null) {
				Matcher annotationNameLineMatcher = annotationNameLinePattern.matcher(line);
				if (annotationNameLineMatcher.matches()) {
					if (annotationNameLineMatcher.group(2).equalsIgnoreCase("CDS")) {
						annotationRecord = new AnnotationRecord(annotationNameLineMatcher.group(2), false);
						annotationRecord.setLocationPattern(annotationNameLineMatcher.group(4));
						annotationRecords.add(annotationRecord);
						analyseRegion(rawSequence, "", annotationNameLineMatcher.group(4), annotationRecord);
					}
				}
				Matcher qualifierMatcher = qualifierPattern.matcher(line);
				if (qualifierMatcher.find()) {
					if (annotationRecord != null) {
						annotationRecord.getQualifiers().add(qualifierMatcher.group(2));
					}
				}
			}
		} catch (Exception e) {
			String message = "Error parsing annotations from " + filePath + ". " + e.getMessage();
			throw new Exception(message);
		}
		if (bufferedReader != null) {
			bufferedReader.close();
		}
		return annotationRecords;
	}

	private static String extractPartSequence(String rawSequence, Integer begin, Integer end) {
		if (end > rawSequence.length()) {
			return rawSequence.substring(begin - 1);
		}
		return rawSequence.substring(begin - 1, end);
	}

	private static void analyseRegion(String rawSequence, String regionSpecifier, String region,
			AnnotationRecord annotationRecord) {
		Pattern specialRegionPattern = Pattern.compile("(complement|join)\\(([a-z0-9.,\\(\\)]+)\\)");
		Pattern simpleRegionPattern = Pattern.compile("([0-9]+)\\.\\.([0-9]+)");
		Pattern joinPattern = Pattern.compile(
				"((complement|join)?\\(?[0-9]+(\\.{2}|\\^)[0-9]+\\)?)(,((complement|join)?\\(?([0-9]+(\\.{2}|\\^)[0-9]+\\)?)))+");

		Matcher specialRegionMatcher = specialRegionPattern.matcher(region);
		Matcher simpleRegionMatcher = simpleRegionPattern.matcher(region);
		Matcher joinRegionMatcher = joinPattern.matcher(region);

		if (simpleRegionMatcher.matches()) { // 12345..12545
			String partSequence = annotationRecord.getPartSequence();
			String newPartSequence = extractPartSequence(rawSequence, new Integer(simpleRegionMatcher.group(1)),
					new Integer(simpleRegionMatcher.group(2)));
			JoinPartItem jpi = new JoinPartItem();
			jpi.setStartPosition(new Integer(simpleRegionMatcher.group(1)));
			jpi.setEndPosition(new Integer(simpleRegionMatcher.group(2)));

			jpi.setSequenceOriginal(newPartSequence);
			
			if (regionSpecifier.equalsIgnoreCase("complement") || annotationRecord.getIsReverseComplement() == true) {
				newPartSequence = SequenceCalculator.calculateReverseComplement(newPartSequence);
				jpi.setIsReverseComplement(true);
			}
			jpi.setSequence(newPartSequence);
			partSequence += newPartSequence;
			annotationRecord.setPartSequence(partSequence);
			annotationRecord.getJoinPartItems().add(jpi);
		} else if (regionSpecifier.equalsIgnoreCase("join")) {
			if (joinRegionMatcher.matches()) {
				String[] joinParts = joinRegionMatcher.group(0).split(",");
				if (annotationRecord.getIsReverseComplement() == true) {
					for (int i = joinParts.length - 1; i >= 0; i--) {
						analyseRegion(rawSequence, "", joinParts[i], annotationRecord);
					}
				} else {
					for (int i = 0; i < joinParts.length; i++) {
						analyseRegion(rawSequence, "", joinParts[i], annotationRecord);
					}
				}
			}
		} else if (specialRegionMatcher.matches()) { // join(....) or complement(....)
			if (regionSpecifier.equalsIgnoreCase("complement")) {
				annotationRecord.setIsReverseComplement(true);
			}
			analyseRegion(rawSequence, specialRegionMatcher.group(1), specialRegionMatcher.group(2), annotationRecord);
		}
	}

}
