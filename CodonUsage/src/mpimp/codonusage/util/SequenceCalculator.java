/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.util;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mpimp.codonusage.db.AnnotationRecord;
import mpimp.codonusage.db.CodonCounter;
import mpimp.codonusage.db.CodonRecord;
import mpimp.codonusage.db.JoinPartItem;

public class SequenceCalculator {

	public static String calculateReverseComplement(String sequence) {
		String reverseComplement = "";
		for (int i = sequence.length(); i > 0; i--) {
			String nucleotide = sequence.substring(i - 1, i);
			reverseComplement += calculateReverseBase(nucleotide);
		}
		return reverseComplement;
	}

	private static String calculateReverseBase(String base) {
		String reverseBase = "";
		if (base.equalsIgnoreCase("A")) {
			reverseBase = "T";
		} else if (base.equalsIgnoreCase("G")) {
			reverseBase = "C";
		} else if (base.equalsIgnoreCase("C")) {
			reverseBase = "G";
		} else if (base.equalsIgnoreCase("T")) {
			reverseBase = "A";
		}
		return reverseBase;
	}

	private static List<CodonRecord> splitSequenceIntoCodons(AnnotationRecord annotationRecord,
			CodonCounter positionInAnalysisSequence) {
		Color displayColor = annotationRecord.getDisplayColor();
		CodonCounter positionInPartSequence = new CodonCounter(1);
		List<CodonRecord> codonRecords = new ArrayList<CodonRecord>();
		List<JoinPartItem> joinPartItems = annotationRecord.getJoinPartItems();
		Collections.sort(joinPartItems, new JoinPartItemComparator(annotationRecord.getIsReverseComplement()));
		String remainingBases = "";
		Integer startPositionOffset = 0;
		CodonRecord borderCodonRecord = null;
		Integer jpiCounter = 0;
		Integer noOfJpis = joinPartItems.size();
		for (JoinPartItem jpi : joinPartItems) {
			jpiCounter++;
			String sequence = jpi.getSequence();
			sequence = sequence.trim();
			sequence = sequence.toUpperCase();
			if (!remainingBases.equals("") && borderCodonRecord != null) {
				// preceeding sequence part could not be completely split into codons
				// so we add the rest to the new sequence part
				sequence = remainingBases + sequence;
			}
			Integer remainingBasesCount = sequence.length() % 3;
			String codon = "";
			CodonRecord codonRecord;
			Integer positionInGenome = 0;
			if (remainingBasesCount == 0 && borderCodonRecord == null) {
				for (int i = 0; i < sequence.length() - 2; i += 3) {
					codon = sequence.substring(i, i + 3);
					codonRecord = new CodonRecord();
					codonRecord.setFromReverseComplement(jpi.getIsReverseComplement());
					codonRecord.setCodon(codon);
					codonRecord.setPositionInPartSequence(positionInPartSequence.getValueAndIncrement());
					codonRecord.setPositionInAnalysisSequence(positionInAnalysisSequence.getValueAndIncrement());
					codonRecord.setDisplayColor(displayColor);
					if (jpi.getIsReverseComplement() == false) {
						positionInGenome = jpi.getStartPosition() + i;
					} else {
						positionInGenome = jpi.getEndPosition() - i;
					}
					codonRecord.setPositionInGenome(positionInGenome);
					codonRecord.setAnnotationOfSourceSequence(annotationRecord.toString());
					codonRecords.add(codonRecord);
				}
			} else if (remainingBasesCount == 0 && borderCodonRecord != null) {
				codon = sequence.substring(0, 3);
				borderCodonRecord.setCodon(codon);
				codonRecords.add(borderCodonRecord);
				borderCodonRecord = null;
				for (int i = 3; i < sequence.length() - 2; i += 3) {
					codon = sequence.substring(i, i + 3);
					codonRecord = new CodonRecord();
					codonRecord.setFromReverseComplement(jpi.getIsReverseComplement());
					codonRecord.setCodon(codon);
					codonRecord.setPositionInPartSequence(positionInPartSequence.getValueAndIncrement());
					codonRecord.setPositionInAnalysisSequence(positionInAnalysisSequence.getValueAndIncrement());
					codonRecord.setDisplayColor(displayColor);
					if (jpi.getIsReverseComplement() == false) {
						positionInGenome = jpi.getStartPosition() + i - startPositionOffset;
					} else {
						positionInGenome = jpi.getEndPosition() - i + startPositionOffset;
					}
					codonRecord.setPositionInGenome(positionInGenome);
					codonRecord.setAnnotationOfSourceSequence(annotationRecord.toString());
					codonRecords.add(codonRecord);
				}

			} else if (remainingBasesCount != 0 && borderCodonRecord == null) {
				startPositionOffset = remainingBasesCount;
				Integer codonRegionLength = sequence.length() - remainingBasesCount;
				remainingBases = jpi.getSequence().substring(jpi.getSequence().length() - remainingBasesCount,
						jpi.getSequence().length());
				int i = 0;
				for (; i < codonRegionLength - 2; i += 3) {
					codon = sequence.substring(i, i + 3);
					codonRecord = new CodonRecord();
					codonRecord.setFromReverseComplement(jpi.getIsReverseComplement());
					codonRecord.setCodon(codon);
					codonRecord.setPositionInPartSequence(positionInPartSequence.getValueAndIncrement());
					codonRecord.setPositionInAnalysisSequence(positionInAnalysisSequence.getValueAndIncrement());
					codonRecord.setDisplayColor(displayColor);
					if (jpi.getIsReverseComplement() == false) {
						positionInGenome = jpi.getStartPosition() + i;
					} else {
						positionInGenome = jpi.getEndPosition() - i;
					}
					codonRecord.setPositionInGenome(positionInGenome);
					codonRecord.setAnnotationOfSourceSequence(annotationRecord.toString());
					codonRecords.add(codonRecord);
				}
				// there are 1 or 2 bases left - we make a new codon record for them (if we are
				// not already at the end of the sequence)
				if (jpiCounter < noOfJpis) {
					borderCodonRecord = new CodonRecord();
					borderCodonRecord.setFromReverseComplement(jpi.getIsReverseComplement());
					borderCodonRecord.setPositionInPartSequence(positionInPartSequence.getValueAndIncrement());
					borderCodonRecord.setPositionInAnalysisSequence(positionInAnalysisSequence.getValueAndIncrement());
					borderCodonRecord.setAnnotationOfSourceSequence(annotationRecord.toString());
					borderCodonRecord.setDisplayColor(displayColor);
					if (jpi.getIsReverseComplement() == false) {
						positionInGenome += 3;
					} else {
						positionInGenome -= 3;
					}
					borderCodonRecord.setPositionInGenome(positionInGenome);
				}
			} else if (remainingBasesCount != 0 && borderCodonRecord != null) {
				startPositionOffset = remainingBasesCount;
				codon = sequence.substring(0, 3);
				borderCodonRecord.setCodon(codon);
				codonRecords.add(borderCodonRecord);
				borderCodonRecord = null;
				Integer codonRegionLength = sequence.length() - remainingBasesCount;
				remainingBases = jpi.getSequence().substring(jpi.getSequence().length() - remainingBasesCount,
						jpi.getSequence().length());
				int i = 0;
				for (; i < codonRegionLength - 2; i += 3) {
					codon = sequence.substring(i, i + 3);
					codonRecord = new CodonRecord();
					codonRecord.setFromReverseComplement(jpi.getIsReverseComplement());
					codonRecord.setCodon(codon);
					codonRecord.setPositionInPartSequence(positionInPartSequence.getValueAndIncrement());
					codonRecord.setPositionInAnalysisSequence(positionInAnalysisSequence.getValueAndIncrement());
					codonRecord.setDisplayColor(displayColor);
					if (jpi.getIsReverseComplement() == false) {
						positionInGenome = jpi.getStartPosition() + i - startPositionOffset;
					} else {
						positionInGenome = jpi.getEndPosition() - i + startPositionOffset;
					}
					codonRecord.setPositionInGenome(positionInGenome);
					codonRecord.setAnnotationOfSourceSequence(annotationRecord.toString());
					codonRecords.add(codonRecord);
				}
				// there are 1 or 2 bases left - we make a new codon record for them (if we are
				// not already at the end of the sequence)
				if (jpiCounter < noOfJpis) {
					borderCodonRecord = new CodonRecord();
					borderCodonRecord.setFromReverseComplement(jpi.getIsReverseComplement());
					borderCodonRecord.setPositionInPartSequence(positionInPartSequence.getValueAndIncrement());
					borderCodonRecord.setPositionInAnalysisSequence(positionInAnalysisSequence.getValueAndIncrement());
					borderCodonRecord.setAnnotationOfSourceSequence(annotationRecord.toString());
					borderCodonRecord.setDisplayColor(displayColor);
					if (jpi.getIsReverseComplement() == false) {
						positionInGenome += 3;
					} else {
						positionInGenome -= 3;
					}
					borderCodonRecord.setPositionInGenome(positionInGenome);
				}
			}
		}
		return codonRecords;
	}

	public static List<CodonRecord> splitSequenceIntoCodons(List<AnnotationRecord> sequencesList) {
		List<CodonRecord> codonRecords = new ArrayList<CodonRecord>();
		CodonCounter positionInAnalysisSequence = new CodonCounter(1);
		for (AnnotationRecord currentRecord : sequencesList) {
			codonRecords.addAll(splitSequenceIntoCodons(currentRecord, positionInAnalysisSequence));
		}
		return codonRecords;
	}

	public static String trimStringToTripletString(String rawString) {
		String filteredRawString = "";
		if (rawString != null && !rawString.equals("")) {
			rawString = rawString.trim();
			Integer rawStringLength = rawString.length();
			Integer noOfTriplets = rawStringLength / 3;
			filteredRawString = rawString.substring(0, noOfTriplets * 3);
			filteredRawString = filteredRawString.toUpperCase();
		}
		return filteredRawString;
	}
}
