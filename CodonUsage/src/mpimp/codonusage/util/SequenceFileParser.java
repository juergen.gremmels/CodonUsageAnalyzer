/*
CodonUsageAnalyzer: A tool for calculating the codon usage (codon adaptation
index, CAI) for selected genes with and without relation to a reference 
genome (or even parts of a reference genome). Input Data may be provided 
as Genbank files (.gb, .gbk), FASTA files (.fa, .fasta) or pasted as plain
text. Results can be exported in MS Excel format.

See also: R. Jansen, H.J. Bussemaker, M. Gerstein; Nucleic Acids Research, 
Volume 31, Issue 8, 15 April 2003, Pages 2242�2251 
(https://doi.org/10.1093/nar/gkg306)

Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

CodonUsageAnalyzer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/
package mpimp.codonusage.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SequenceFileParser {

	public static String extractSequenceFromGenebankFile(String filePath)
			throws IOException {
		String output = "";
		Pattern patternOrigin = Pattern.compile("ORIGIN");
		Pattern pattern = Pattern.compile("(.+\\d+)([ATGCatgc ]*)");
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(filePath));
			String line = "";
			String seqLine = "";
			Boolean inSequencePart = false;
			while ((line = reader.readLine()) != null) {
				Matcher matcherOrigin = patternOrigin.matcher(line);
				if (matcherOrigin.find()) {
					inSequencePart = true;
				}
				Matcher matcher = pattern.matcher(line);
				if (matcher.find() && inSequencePart == true) {
					seqLine = matcher.group(2);
					seqLine = seqLine.replace(" ", "");
					output += seqLine;
				}
			}

			reader.close();
		} catch (IOException ioe) {
			if (reader != null) {
				reader.close();
			}
			throw new IOException(ioe);
		}
		if (reader != null) {
			reader.close();
		}
		return output;
	}
	
	public static String extractSequenceFromFastaFile(String filePath) throws IOException  {
		String output = "";
		Pattern sequenceLinePattern = Pattern.compile("[AaGgCcTt]+");
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(filePath));
			String line = "";
			while ((line = reader.readLine()) != null) {
				Matcher sequenceLineMatcher = sequenceLinePattern.matcher(line);
				if (sequenceLineMatcher.matches()) {
					line = line.trim();
					line = line.toUpperCase();
					output += line;
				}
			}
		} catch (IOException ioe) {
			if (reader != null) {
				reader.close();
			}
			throw new IOException(ioe);
		}
		if (reader != null) {
			reader.close();
		}
		return output;
	}
}
